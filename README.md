# ROTOR: A Reliable OCaml Tool for OCaml Refactoring - Trustworthy Refactoring for OCaml

ROTOR is a tool for refactoring OCaml code, written in OCaml.

The eventual aim is for ROTOR to be able to not only perform automatic
refactoring, but also integrate formal verification that the refactorings are
**correct**. It is intended that the [CakeML](https://cakeml.org/) project will
be used for this.

More details can be found at the project
[website](https://www.cs.kent.ac.uk/projects/trustworthy-refactoring).

## Dependencies

* The [containers](https://github.com/c-cube/ocaml-containers) package
  (version >=1.1).

* The [ocamlgraph](http://ocamlgraph.lri.fr/index.en.html) package
  (version >= 1.8.7).

* The [logs](http://erratique.ch/software/logs) package
  (version >=0.6.2).

* The [mparser](https://github.com/cakeplus/mparser) (version >= 1.2.3) and
  [pcre](http://mmottl.github.io/pcre-ocaml) packages.

* The [visitors](https://gitlab.inria.fr/fpottier/visitors) package
  (version >= 20170317).
  The Visitors package itself requires `ppx_tools` and `ppx_deriving`.

* The `diff` and `filterdiff` utilities; `diff` usually comes as standard,
  but `filterdiff` can be found in the `patchutils` package.

* The `rlwrap` utility, for running the `utils/toplevel.sh` script.

* An external library wrapping up the OCaml compiler. This approach has
  been taken so that modifications to the compiler required by the tool's
  development can easily be integrated. The current setup requires this
  library to be called "devcompiler" and be installed in a location visible
  to the ocaml development environment (e.g. OPAM). To avoid name clashes with
  the standard compiler modules, the library should expose modules in the:

  * `utils` subdirectory in the `Ocaml_utils` umbrella module
  * `parsing` subdirectory in the `Ocaml_parsing` umbrella module
  * `typing` subdirectory in the `Ocaml_typing` umbrella module
  * `driver` subdirectory in the `Driver` umbrella module

  The settings in the Ocaml_utils.Config module should reflect the values
  found in the system compiler - in particular it should have the correct path
  to the standard library.

  The tool is currently compatible with the version 4.0.4 release of the
  compiler.

* The Jane Street testbed code requires the following packages:

  * [ocaml-migrate-parsetree](https://github.com/let-def/ocaml-migrate-parsetree)
  * [octavius](https://github.com/ocaml-doc/octavius)
  * [re](https://github.com/ocaml/ocaml-re)

  Building the testbed requires
  [jbuilder](https://github.com/janestreet/jbuilder) (version <= 1.0+beta11).

  The testbed tarball is versioned via [Git LFS](https://git-lfs.github.com/),
  which you will need to have installed in order to download it from the
  repository.

---

## Test Suite

The test suites can be run by calling `make` with targets having the prefix
`tests` (e.g. `tests.jane-street.all`).
See the [Test Suite README](test/README.md) for more details.

---

## Progress Log

### 15 January 2018

* Added 'parameter' as a logical entity; in particular this is being used to
  refer to functor parameters.

### 30 December 2017

* We model a distinction between structures and functors. Note that both may be
  bound to modules. This distinction is represented in the rich identifiers used
  by the tool.

### 17 September 2017

* Created a Dockerfile for building a portable Docker image of the tool.

### 08 September 2017

* Presented the tool at the
  [OCaml Users Workshop 2017](https://ocaml.org/meetings/ocaml/2017/).

### 07 August 2017

* Refined renaming of values into two distinct refactorings: renaming a value in
  an implementation and interface (a named module type) respectively. In fact,
  this dichotomy will possibly be a facet of all refactorings. Finished
  implementing first veresion of both kinds of value renaming. Next step is to
  make these refactorings take functors into account.

* Extracted all persistent identifiers used the in the Jane Street test bed.
  This collection can be used to stress test the value renaming refactoring.

### 25 June 2017

* A rich representation of identifiers implemented, allowing the language
  __kind__ (e.g. value, module, module type, etc.) of each segment of a
  long identifier to be specified. The module also encapsulates the valid
  nestings of each kind (e.g. a module can contain a value, but not vice-versa).

  Currently only modules, module types, and values are supported.

### 24 May 2017

* Automated framework for running test suites set up. Tests can specify what the
  resulting diff is expected to be in order to determine success or failure of
  the test case.
* Tests can specify that they expect the refactoring tool to fail, and also
  which exit code is expected (26 May 2017).

### 19 May 2017

* Renaming detects when shadowing may occur.

### 11 May 2017

* Dependencies between source files are now computed and refactorings only run
  on certain files based on these dependencies. Refactoring dependencies also
  use these file dependencies. We now rely on the concept of a "kernel" for a
  refactoring: this is the set of files in a codebase for which we only need
  look at one level of file dependencies in order to correctly apply the
  refactoring.

### 03 April 2017

* Set up logging infrastructure using the Logs package.

### 30 March 2017

* Added facility to compute refactoring dependencies. Computation of
  dependencies for renaming of values still work in progress. Main executable
  now outputs diffs directly.

### 19 Mar 2017

* Updated to use official release of Visitors package, which now supports
  `[@name]` and `[@build]` attributes.

### 02 Mar 2017

* Added shell script to run refactorings over given directories and store diffs
  in files.

### 01 Mar 2017

* Typed ASTs are now read from .cmt files when they are present.
* Implemented basic renaming of values. The value to be renamed is specified
  using a long identifier (e.g. Foo.Bar.baz), and function argument and record
  field punning is taken into account. It is not yet correct in all cases: e.g.,
  if the parent module of a renamed value is included in another module M, then
  the refactorer does not automatically rename the signature of M, nor does it
  yet issue a suitable warning. The aim is that such "dependencies" be
  calculated and checked in future.

### 24 Feb 2017

* Common frontend functionality, with PPX processing.

### 22 Feb 2017

* Packaged all modules into a library and created scripts to initiate a toplevel
  loop and load the library.

### 9 Feb 2017

* Interface for refactorings as modules created.
* Implemented first basic refactoring (rename an expression identifier)!

### 7 Feb 2017

* Visitors can be generated for the Parsetree and Typedtree types.

---

## Language featuers which are not yet handled

* Functors (in progress).
* The `typeof` construct.
* First-class modules.
* Local module bindings (within value expressions).
* Not quite sure what the `Tmty_alias` case in module types represents.