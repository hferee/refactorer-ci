open Containers

open Compiler

open Clflags
open Config

open Format

open Configuration

open Sourcefile

module Option = Containers.Option

open Containers.IO

let input = ref None
let inputs = ref []
let lib_name = ref None
let ppx  = ref []

let intf = ref false
let module_name = ref None

let codebase = ref Codebase.empty
let dependencies_in = ref None
let dependencies_out = ref None

let add_previous () =
  !input |> Option.iter @@ function
  | `File f ->
    inputs := `File (f, !lib_name, !ppx) :: !inputs
  | `Dir dir ->
    inputs := `Dir (dir, !lib_name, !ppx) :: !inputs

let add_previous () =
  add_previous () ;
  input := None ;
  lib_name := None ;
  ppx := []

let add_file f =
  let () = add_previous () in
  input := Some (`File f)

let add_directory d =
  let () = add_previous () in
  input := Some (`Dir d)

let speclist = [
    ("-I", Arg.String (fun dir -> include_dirs := dir :: !include_dirs),
      "<dir>: Add <dir> to the list of include directories.") ;
    ("-get-deps-from-file", Arg.String (fun f -> dependencies_in := Some f),
      "<file>: Load source file dependencies from GML representation in <file>.") ;
    ("-dump-deps-to-file", Arg.String (fun f -> dependencies_out := Some f),
      "<file>: Output GML representation of source file dependencies to <file>.") ;
    ("-f", Arg.String add_file, "<file>: add <file> to the input codebase.") ;
    ("-d", Arg.String add_directory, "<dir>: add <dir> to the input codebase.") ;
    ("-ppx", Arg.String (fun _ppx -> ppx := _ppx :: !ppx),
      "<command>: Pipe abstract syntax tree through preprocessor <command>.") ;
    ("-lib", Arg.String (fun lib -> lib_name := Some lib),
      "<lib-name>: set library to which input file(s) belongs.") ;
    ("-module", Arg.String (fun m -> module_name := Some m),
      "<mod>: use <mod> as the toplevel module name for standard input.") ;
    ("-intf", Arg.Set intf, ": process standard input as an interface file.") ;
    ("-impl", Arg.Clear intf,
      ": process standard input as an implementation file (default).") ;
  ]

let usage =
    " [-I <dir>]* [-get-deps-from-file <file>] [-dump-deps-to-file <file>] \
      [( (-f <file> | -d <dir>) [-lib <name>] (-ppx <command>)* )* \
         | [-module <mod>] [-impl | -intf] [-lib <name>] (-ppx <command>)* ]"

let prng_state = Random.State.make_self_init ()

(* Adapted from Jane Street Core_filename module (tmp_dir and retry). *)
let tmp_dir () =
  let rec try_name counter =
    let name =
      sprintf "%s_%06x"
        !tool_name (Random.State.bits prng_state) in
    let name = Filename.concat (Filename.get_temp_dir_name ()) name in
    try
      Unix.mkdir name 0o700 ; name
    with Sys_error _ | Unix.Unix_error _ as e ->
      if counter >= 1000 then raise e else try_name (counter + 1)
  in
  try_name 0

let add_stdin () =
  let module_name = match !module_name with
    | None ->
        failwith "A module name must be specified when using standard input!"
    | Some x -> x in
  let ext =
    if !intf then !interface_suffix else !implementation_suffix in
  let tmp_file =
    Filename.concat (tmp_dir ()) (sprintf "%s.%s" module_name ext) in
  let tmp_file_out = open_out tmp_file in
  let () = output_string tmp_file_out (read_all stdin) in
  let () = close_out tmp_file_out in
  inputs :=  `File (tmp_file, !lib_name, !ppx) :: !inputs

let codebase () =
  let () = if Option.is_none !input then add_stdin () else add_previous () in
  let () =
    !inputs
    |> List.iter @@ function
    | `File (f, lib_name, ppx) ->
      begin try
        codebase := Codebase.add (Fileinfos.mk (f, lib_name, ppx)) !codebase
      with Not_found ->
        Logging.warn @@ fun _f -> _f "%s not found!" f
      end
    | `Dir (dir, lib, ppx) ->
      begin try
        codebase := Codebase.add_directory ~lib ~ppx ~dir !codebase
      with Not_found ->
        Logging.warn @@ fun _f -> _f "%s not found!" dir
      end in
  !codebase

let codebase = lazy (codebase ())

let file_dependencies =
  lazy begin
    let deps =
      match !dependencies_in with
      | Some f ->
        let () =
          Logging.progress_statement @@ fun _f -> _f
            "Reading file dependencies:" in
        Fileinfos.Graph.parse f
          |> Fun.tap (fun _ -> Logging.major_progress ())
      | None ->
        Codebase.dependency_graph (Lazy.force codebase) in
    let () =
      match !dependencies_out with
      | None -> ()
      | Some f ->
        with_out f @@ fun f ->
          write_line f (sprintf "%a" Fileinfos.Graph.print deps) in
    deps
  end