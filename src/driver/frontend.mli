(** This module encapsulates the state and operations for reading command line
    arguments and generating a [Codebase.t] value. *)

(* TODO: Just make this part of Codebase? *)

val usage : string
(** A usage string describing the arguments specified by [speclist]. *)

val speclist : (string * Arg.spec * string) list
(** Spec list suitable for use with command line processing functions in the
    [Arg] module. *)

val codebase : Codebase.t Lazy.t
(** Lazily computes the codebase specified by the internal state of the module,
    which is set by processing the command line arguments using [speclist].
    Therefore forcing [codebase] should only be done after processing the
    command line arguments. *)

val file_dependencies : Fileinfos.Graph.t Lazy.t
(** Lazily computes the dependency graph for [codebase]. *)