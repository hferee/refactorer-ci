(* -------------------------------------------------------------------------- *)
(* Occurrences of the following types in type definitions have been changed   *)
(* to use the types indicated in parentheses so that the visitor generation   *)
(* code creates calls to the correct method.                                  *)
(*      Location.t      (Location_visitors.location_t)                        *)
(* -------------------------------------------------------------------------- *)

open Compiler

module Base = Asttypes

type constant = Base.constant =
    Const_int of int
  | Const_char of char
  | Const_string of string * string option
  | Const_float of string
  | Const_int32 of int32
  | Const_int64 of int64
  | Const_nativeint of nativeint

and rec_flag = Base.rec_flag =
  Nonrecursive | Recursive

and direction_flag = Base.direction_flag =
  Upto | Downto

and private_flag = Base.private_flag =
  Private | Public

and mutable_flag = Base.mutable_flag =
  Immutable | Mutable

and virtual_flag = Base.virtual_flag =
  Virtual | Concrete

and override_flag = Base.override_flag =
  Override | Fresh

and closed_flag = Base.closed_flag =
  Closed | Open

and label = string

and arg_label = Base.arg_label =
    Nolabel
  | Labelled of string (*  label:T -> ... *)
  | Optional of string (* ?label:T -> ... *)

and 'a loc = 'a Location.loc = {
  txt : 'a;
  loc : Location_visitors.location_t;
}

and variance = Base.variance =
  | Covariant
  | Contravariant
  | Invariant

[@@deriving
    visitors { variety = "iter"; polymorphic = true; monomorphic = ["'env"]; ancestors = [ "Location_visitors.iter" ] }
  , visitors { variety = "map"; polymorphic = true; monomorphic = ["'env"]; ancestors = [ "Location_visitors.map" ] }
  , visitors { variety = "reduce"; polymorphic = true; monomorphic = ["'env"]; ancestors = [ "Location_visitors.reduce" ] }

  , visitors { variety = "iter2"; concrete = true; polymorphic = true; monomorphic = ["'env"]; ancestors = [ "Location_visitors.iter2" ] }
]