module Make() :
  sig
    include Refactoring.S
    val init : Elements.Base._value Identifier.t * string -> unit
  end