open Containers

open Compiler

open Asttypes

open Ident
open Longident

open Typedtree

open Types

open Compmisc

open List
open Fun

open Refactoring
open Refactoring_utils
open Refactoring_visitors

open Lib
open Ast_lib
open Ast_utils
open Compiler_utils
open Visitors_lib

open Elements.Base
open Identifier

open Typedtree_views

open Sourcefile
open Fileinfos

open Logging.Tags

open Rename_core

module DepSet = Deps.Elt.Set

include Core

module Make () = struct

  include InputState.Make ()
  include ParameterState.Make (struct
      type kind_t = _value
      let kind = Elements.Base.Value
      let variety = Chain.InImpl
    end)

  (*** Properties ***)

  let name = "Rename implementation value"

  (* ------------------------------------------------------------------------
      REPLACEMENTS
    ------------------------------------------------------------------------- *)

  module Reps = struct

    module Nonlocal = struct

      let visitor (ident, from_id) =
        object(self)

          inherit [_] result_reducer as super

          method! visit_tt_expression pun_tag
              ({ exp_env; exp_loc; exp_desc; exp_extra; _ } as this) =
            let env =
              List.rev exp_extra
                |> find_map @@ function
                | Texp_open (_, _, _, env), _, _ -> Some env
                | _ -> None in
            let env = Option.get_or ~default:exp_env env in
            (* Is it necessary to use this environment, rather than the one
               present in [this]? *)
            match exp_desc with
            | Texp_ident (p, { txt; loc; }, _) ->
              let p = Env.normalize_path (Some loc) env p in
              let head = Path.head p in
              let p =
                if Ident.persistent head then snd (Buildenv.Factorise.path p)
                else p in
              let id =
                Chain.of_path Elements.Base.Value p in
              if not ((Ident.same ident head) && Chain.equal id from_id) then
                self#zero
              else
                (* TODO: We may need to fully qualify the value when generating
                         the replacement, in case the original identifer is not
                         qualified and there is already a binding in scope which
                         is the same as the replacement identifier. *)
                let str =
                  match pun_tag with
                  | `NO_PUN ->
                      let Atom.Data.{ id; _ } =
                        Atom.unwrap (Chain.anchor from_id) in
                      String.replace ~which:`Right ~sub:id ~by:(to_id ())
                        (longident_to_string txt)
                  | `ARG_PUN ->
                      (Tokens.mk_arg_label
                        ~signifier:false (longident_to_string txt)) ^ (to_id ())
                  | `FIELD_PUN ->
                      (Tokens.mk_field_label (longident_to_string txt))
                        ^ (to_id ()) in
                Replacement.Set.of_opt (Replacement.mk_opt loc str)
            | Texp_apply (_fun, args) ->
              let result = self#visit_tt_expression pun_tag _fun in
              (* Check for punning when processing arguments
                ----------------------------------------------------------------
                The type checker fills in missing arguments and rearranges them
                to match the order they are given in the function definition. So
                we need to filter out the ones not given in the source, and then
                sort by location so that we can correctly compute each argument's
                preamble in the source text to check for punning. We also need to
                treat the first argument specially in case the function applied
                is infix. *)
              let process_arg prev_loc ((_, exp) as arg) =
                match exp.exp_desc with
                | Texp_ident _ when not exp.exp_loc.Location.loc_ghost ->
                    begin try
                      let preamble =
                        extract_src (get_input ()) prev_loc exp.exp_loc in
                      let pun_tag =
                        if is_arg_pun preamble arg then `ARG_PUN else `NO_PUN in
                      self#visit_tt_expression pun_tag exp
                    with Invalid_argument s ->
                      let () = Logging.warn @@ fun _f -> _f
                        "@[<v 4>%s@,%a@,%a@]" s
                        Location.print_compact prev_loc
                        Location.print_compact exp.exp_loc in
                      Replacement.Set.empty
                    end
                | _ -> self#visit_tt_expression pun_tag exp in
              let args =
                List.filter_map
                  (fun (lbl, exp) -> (Option.map (Pair.make lbl) exp))
                  args in
              let args =
                List.sort
                  (fun (_, exp) (_, exp') -> cmp_loc exp.exp_loc exp'.exp_loc)
                  args in
              let result, args = match args with
              | [] ->
                result, args
              | ((_, exp) as arg) :: args ->
                if cmp_loc exp.exp_loc _fun.exp_loc < 0 then
                  self#plus result (process_arg exp_loc arg), args
                else
                  result, arg::args in
              let result, _ =
                List.fold_left
                  (fun (result, prev_loc) ((_, exp) as arg) ->
                    (self#plus result (process_arg prev_loc arg)),
                      (if exp.exp_loc.Location.loc_ghost
                        then prev_loc else exp.exp_loc))
                  (result, _fun.exp_loc) args in
              result
            | Texp_record { fields; extended_expression; _ } ->
              let result = match extended_expression with
                | Some exp -> self#visit_tt_expression pun_tag exp
                | None -> Replacement.Set.empty in
              (* Check for punning when processing record fields
                ----------------------------------------------------------------
                The type checker fills in missing fields and rearranges them
                to match the order they are given in the record definition. So
                we need to filter out the ones not given in the source, and then
                sort by location so that we can correctly compute each field's
                preamble in the source text to check for punning. *)
              let process_fld prev_loc ((_, (_, exp)) as fld) =
                match exp.exp_desc with
                | Texp_ident _ when not exp.exp_loc.Location.loc_ghost ->
                    begin try
                      let preamble =
                        extract_src (get_input ()) prev_loc exp.exp_loc in
                      let pun_tag =
                        if is_field_pun preamble fld
                          then `FIELD_PUN else `NO_PUN in
                      self#visit_tt_expression pun_tag exp
                    with Invalid_argument s ->
                      let () = Logging.warn @@ fun _f -> _f
                        "@[<v 4>%s@,%a@,%a@]" s
                        Location.print_compact prev_loc
                        Location.print_compact exp.exp_loc in
                      Replacement.Set.empty
                    end
                | _ -> self#visit_tt_expression pun_tag exp in
              let fields = Array.fold_right
                (fun (lbl_desc, lbl_def) acc -> match lbl_def with
                  | Kept _ -> acc
                  | Overridden (loc, exp) -> (lbl_desc, (loc, exp))::acc)
                fields [] in
              let fields = List.sort
                (fun (_, (_, exp)) (_, (_, exp')) ->
                  cmp_loc exp.exp_loc exp'.exp_loc) fields in
              let result, _ = List.fold_left
                (fun (result, prev_loc) ((_, (_, exp)) as lbl) ->
                  (self#plus result (process_fld prev_loc lbl)), exp.exp_loc)
                (result,
                  (Option.map_or ~default:exp_loc (fun exp -> exp.exp_loc)
                    extended_expression))
                fields in
              result
            | _ ->
              super#visit_tt_expression pun_tag this

          method! visit_tt_include_declaration env decl =
            let { incl_mod; incl_loc; _ } = decl in
            let { mod_loc; mod_env } as me = incl_mod in
            let me, tys = Module.unwrap_expr me in
            match me.mod_desc with
            | Tmod_ident (p, _) ->
              let p = Env.normalize_path (Some mod_loc) mod_env p in
              let head = Path.head p in
              let p =
                if Ident.persistent (Path.head p)
                  then snd (Buildenv.Factorise.path p)
                  else p in
              if not (Ident.same ident head) then
                self#zero
              else
                let p = Chain.of_path Elements.Base.Structure p in
                begin match Chain.drop p from_id with
                | None ->
                  (* [p] should not be equal to [from_id] since the latter
                     refers to a value and the former to a module. *)
                  assert false
                | Some ((Chain.Atomic Atom.Value _) as v)
                    when
                      List.is_empty tys
                        ||
                      let { mty_env; mty_type; _} = List.hd tys in
                      Option.is_some
                        (Moduletype.resolve_lookup mty_env mty_type v) ->
                  begin try
                    let _ = Env.lookup_value (Lident (to_id ())) mod_env in
                    let err =
                      SoundnessViolation
                        (Format.sprintf
                          "Renaming would cause the include at %a to shadow an \
                           existing binding in scope at that location."
                          Location.print_compact incl_loc) in
                    raise (Error (err, None))
                  with
                  | Not_found ->
                    self#zero
                  end
                | _ ->
                  self#zero
                | exception Invalid_argument _ ->
                  self#zero
                end
            | Tmod_apply _
              (* TODO *)
            | _ ->
              super#visit_tt_include_declaration env decl

          (* N.B. No need to do nonlocal renaming in signatures. *)

        end

      end

      module Local = struct

        class ['a] env id scope = object (self)
          inherit ['a] id_env id
          inherit module_scope_env scope
        end

        let pattern_var_loc id =
          let reducer =
            object(self)
              inherit [_] Opt_reducers.leftmost
              method! visit_Tpat_var id id' { loc; _ } =
                if (Ident.same id id') then
                  Some (`Regular loc)
                else
                  None
              method! visit_Tpat_alias id pat as_id { loc; _ } =
                if (Ident.same id as_id) then
                  Some (`Regular loc)
                else
                  self#visit_tt_pattern id pat
              method! visit_Tpat_record id flds _ =
                flds |> List.find_map @@ fun ({ txt; loc; }, _, pat) ->
                  (self#visit_tt_pattern id pat) |> Option.map @@ function
                    | `Regular l when l = loc ->
                      `Punned ((longident_to_string txt), l)
                    | x -> x
            end in
          Option.get_exn % reducer#visit_tt_pattern id

        let check_soundness env mt =
          let to_id = Chain.mk (Atom.mk Value (Atom.Data.only (to_id ()))) in
          if Option.is_some (Moduletype.lookup env mt to_id) then
            let err =
              SoundnessViolation
                (Format.sprintf
                  "Module already contains a binding for %a" Chain.pp to_id) in
            raise (Error (err, None))

        let visitor =
          object(self)

            inherit [_] reduce_with_id as super
            inherit Replacement.Set.monoid

            method! process_binding_from_struct env this
                (type b)
                  ((id : (Ident.t, b) Atom.t), _)
                  (((_, _, InStr { str_env; str_loc; }, binding) as source)
                    : (b, impl) binding_source) =
              let err =
                lazy
                  (invalid_arg
                    (Format.sprintf
                      "%s.replace_local#process_binding_from_struct"
                      __MODULE__)) in
              let sound =
                lazy (check_soundness str_env (Types_views.ST this.str_type)) in
              let locals =
                match id, binding with
                | Atom.StructureType _, _ ->
                  Lazy.force err
                | Atom.FunctorType _, _ ->
                  Lazy.force err
                | _, `Binding Value (_, InStrPrim { val_name = { loc; _}; _ }) ->
                  let () = Lazy.force sound in
                  Replacement.Set.of_opt (Replacement.mk_opt loc (to_id ()))
                | Atom.Value id, `Binding Value (_, InStrVal { vb_pat; _ }) ->
                  let () = Lazy.force sound in
                  begin match pattern_var_loc id vb_pat with
                  | `Regular loc ->
                    Replacement.Set.of_opt (Replacement.mk_opt loc (to_id ()))
                  | `Punned (lbl, loc) ->
                    Replacement.Set.of_opt
                      (Replacement.mk_opt loc
                        ((Tokens.mk_field_label lbl) ^ (to_id ())))
                  end
                | Atom.Value _, `Include InStr decl ->
                  let () = Lazy.force sound in
                  self#visit_tt_include_declaration env decl
                | _, `Binding Structure (_, InStr mb) ->
                  self#visit_tt_module_binding env mb
                | _, `Binding Functor (_, InStr mb) ->
                  self#visit_tt_module_binding env mb
                | _, `Include InStr decl ->
                  self#visit_tt_include_declaration env decl in
              let Atom.Data.{ id; _ } = Atom.unwrap id in
              let nonlocals =
                (Nonlocal.visitor (id, env#id))#visit_tt_structure `NO_PUN
                  {this with
                    str_items =
                      let InStr rest = next_items source in rest } in
              self#plus locals nonlocals

            method! visit_tt_module_binding env mb =
              let id = Chain.tl_exn env#id in
              let scope, _ =
                Modulescope.enter_module_binding (InStr mb) env#scope in
              super#visit_tt_module_binding (new env id scope) mb

            method! visit_tt_include_declaration env decl =
              let scope, _ = Modulescope.enter_include (InStr decl) env#scope in
              super#visit_tt_include_declaration (env#with_scope scope) decl

            method! visit_tt_module_expr env this =
              match this.mod_desc with
              | Tmod_functor (x, _, x_type, body) ->
                self#visit_functor env x x_type (InStr body)
              | Tmod_apply _ ->
                let () = Logging.warn @@ fun _f -> _f
                  ~tags:(of_loc this.mod_loc)
                  "module bound to a functor application - cannot do local renaming!" in
                self#zero
              | Tmod_unpack _ ->
                let () = Logging.warn @@ fun _f -> _f
                  ~tags:(of_loc this.mod_loc)
                  "module bound to an expression unpacking - cannot do local renaming!" in
                self#zero
              | _ ->
                super#visit_tt_module_expr env this

            method! process_binding_from_sig env this
                (type b)
                  ((id : (Ident.t, b) Atom.t), _)
                  ((_,_, InSig { sig_env; _ }, binding)
                    : (b, intf) binding_source) =
              let err =
                lazy
                  (invalid_arg
                    (Format.sprintf
                      "%s.replace_local#process_binding_from_sig"
                      __MODULE__)) in
              let sound =
                lazy (check_soundness sig_env (Types_views.ST this.sig_type)) in
              match id, binding with
              | Atom.StructureType _, _ ->
                Lazy.force err
              | Atom.FunctorType _, _ ->
                Lazy.force err
              | Atom.Value id,
                `Binding Value (_, InSig { val_name = { loc; _}; _ }) ->
                let () = Lazy.force sound in
                Replacement.Set.of_opt (Replacement.mk_opt loc (to_id ()))
              | Atom.Value _, `Include InSig desc ->
                let () = Lazy.force sound in
                self#visit_tt_include_description env desc
              | _, `Binding Structure (_, InSig md) ->
                self#visit_tt_module_declaration env md
              | _, `Binding Functor (_, InSig md) ->
                self#visit_tt_module_declaration env md
              | _, `Include InSig desc ->
                self#visit_tt_include_description env desc

            method! visit_tt_module_declaration env md =
              let id = Chain.tl_exn env#id in
              let scope, _ =
                Modulescope.enter_module_binding (InSig md) env#scope in
              super#visit_tt_module_declaration (new env id scope) md

            method! visit_tt_include_description env desc =
              let scope, _ = Modulescope.enter_include (InSig desc) env#scope in
              super#visit_tt_include_description (env#with_scope scope) desc

            method! visit_tt_module_type env this =
              match this.mty_desc with
              | Tmty_functor (x, _, x_type, body) ->
                self#visit_functor env x x_type (InSig body)
              | Tmty_typeof _ ->
                let () = Logging.warn @@ fun _f -> _f
                  ~tags:(of_loc this.mty_loc)
                  "module has type of a module expression - cannot do local renaming!" in
                self#zero
              | _ -> super#visit_tt_module_type env this

          method visit_functor
              : 'a . _ -> _ -> _ -> 'a Typedtree_views.module_expr_view -> _ =
            fun (env : Elements.Base._value env) x x_type
                (type a) (body : a Typedtree_views.module_expr_view) ->
              let num_params =
                List.length (Modulescope.peek_params env#scope) + 1 in
              match Chain.hd env#id with
              | Atom.Ex Atom.Parameter (_, None) ->
                (* We expect identifier construction to force functor
                   parameters to be indexed. *)
                assert false
              | Atom.Ex Atom.Parameter (_, Some idx) when idx = num_params ->
                begin match x_type with
                | None ->
                  failwith
                    (Format.sprintf
                      "%s.Reps.Local.visitor#visit_functor: cannot enter \
                       unit functor parameter"
                      __MODULE__)
                | Some x_type ->
                  let locals =
                    let id = Chain.tl_exn env#id in
                    let scope = Modulescope.enter_param x env#scope in
                    self#visit_tt_module_type (new env id scope) x_type in
                  let nonlocal_visitor = Nonlocal.visitor (x, env#id) in
                  let nonlocals =
                    match body with
                    | InSig body ->
                      nonlocal_visitor#visit_tt_module_type `NO_PUN body
                    | InStr body ->
                      nonlocal_visitor#visit_tt_module_expr `NO_PUN body in
                  self#plus locals nonlocals
                end
              | _ ->
                match body with
                | InStr body ->
                  let _, tys = Module.unwrap_expr body in
                  let param = Option.map (Pair.make x) x_type in
                  let scope = Modulescope.push_param (param, tys) env#scope in
                  self#visit_tt_module_expr (env#with_scope scope) body
                | InSig body ->
                  let param = (Option.map (Pair.make x) x_type, []) in
                  let env =
                    env#with_scope (Modulescope.push_param param env#scope) in
                  self#visit_tt_module_type env body

        end

    end

  end

  let lookup_head_ident cont =
    let env = Env.empty in
    let Atom.Ex hd = Chain.hd (from_id ()) in
    let path =
      lookup_path
        ~lookup_f:(fun lid -> Env.lookup_module ~load:true lid env)
        ?backup_lib:((get_input ()).fileinfos.library)
        ~env
        (from_lib (), Longident.Lident Atom.((unwrap hd).Data.id)) in
    match path with
    | None ->
      cont None
    | Some (Path.Pident hd) ->
      cont (Some hd)
    | _ ->
      assert false

  let process_nonlocal cont =
    lookup_head_ident (Option.map_or ~default:Replacement.Set.empty cont)

  let process_sig local scope _sig =
    if local then
      let from_id = Chain.tl_exn (from_id ()) in
      Reps.Local.(visitor#visit_tt_signature (new env from_id scope) _sig)
    else
      process_nonlocal
        (fun ident ->
          let visitor = Reps.Nonlocal.visitor (ident, from_id ()) in
          visitor#visit_tt_signature `NO_PUN _sig)

  let process_struct local scope _struct =
    if local then
      let from_id = Chain.tl_exn (from_id ()) in
      let replacements =
        Reps.Local.
          (visitor#visit_tt_structure (new env from_id scope) _struct) in
      let () = if Replacement.Set.is_empty replacements then
        (* TODO: Consider: do we need to emit this warning? *)
        Logging.warn @@ fun _f -> _f
          "Could not find local value to rename in %s!"
          (get_input ()).fileinfos.filename in
      replacements
    else
      process_nonlocal
        (fun ident ->
          let visitor = Reps.Nonlocal.visitor (ident, from_id ()) in
          visitor#visit_tt_structure `NO_PUN _struct)

  let process_file input =
    let infos = input.fileinfos in
    let local = is_local infos in
    let mod_scope = ((module_name infos, None), []) in
    let () = set_input input in
    dispatch_on_input_type
      ~intf_f:(process_sig local mod_scope)
      ~impl_f:(process_struct local mod_scope)


  (* ------------------------------------------------------------------------
      DEPENDENCIES
    ------------------------------------------------------------------------- *)
  module Deps = struct

    module Nonlocal = struct

      (* TODO: Decide how to handle anonymous scope frames *)
      let visitor (ident, id_tail) =
        object(self)

          inherit [_] dep_reducer as super

          (* This will control the direction of recursion through structures.
             For now, we want to rule out recursing into certain structure
             items (e.g. values/evals) in order to avoid generating bogus
             dependencies. *)
          (* TODO: work out what needs doing (if anything) in different kinds of
                   structure items. *)
          method! visit_tt_structure_item_desc scope this =
            match this with
            | Tstr_eval _
            | Tstr_value _
            | Tstr_primitive _
            | Tstr_type _
            | Tstr_typext _
            | Tstr_exception _
            | Tstr_open _
            | Tstr_class _
            | Tstr_class_type _
            | Tstr_attribute _
            ->
              self#zero
            | Tstr_modtype _
              (* TODO: We **do** want to recurse into module types, e.g. in the
                 Jane Street testbed, in the renaming

                   base:Char.hash_fold_t -> foo

                 the following dependency is generated:

                   base:Type_equal:Id:Uid.hash_fold_t -> foo

                 and the module base:Type_equal:Id:Uid is referred to in the
                 core_kernel:Type_equal_intf module as follows:

                   module type Uid = sig
                     include
                       module type of
                         struct include Base.Type_equal.Id.Uid end
                     ...
                   end

                 So here we need to generate the dependency

                   core_kernel:Type_equal_intf%Uid.hash_fold_t

                 However, this is a little more complicated than simply recursing
                 into module types, since it also uses the module type of ...
                 construct. *)
            ->
              self#zero
            | Tstr_module _
            | Tstr_recmodule _
            | Tstr_include _
            ->
              super#visit_tt_structure_item_desc scope this

          method! visit_tt_module_binding scope mb =
            let scope, (InStr me) =
              Modulescope.enter_module_binding (InStr mb) scope in
            self#visit_tt_module_expr scope me

          method! visit_tt_include_declaration scope decl =
            let scope, (InStr me) =
              Modulescope.enter_include (InStr decl) scope in
            self#visit_tt_module_expr scope me

          method! visit_tt_module_expr scope me =
            match me.mod_desc with
            | Tmod_constraint _ ->
              (* Shouldn't get here as the [Modulescope.enter_] functions should
                  have stripped all the constraints off before recursing. *)
              assert false
            | Tmod_ident (p, _) ->
              let mod_sort =
                Option.map
                  Elements.(Module._lift % ModuleType.type_of)
                  (Moduletype.sort me.mod_env me.mod_type) in
              begin match mod_sort with
              | None ->
                let () = Logging.info @@ fun _f -> _f
                  ~tags:(of_loc me.mod_loc)
                  "Could not determine sort of module expression: \
                   cannot check nonlocal dependencies" in
                self#zero
              | Some Elements.Base.Ex mod_sort ->
                let p = Env.normalize_path (Some me.mod_loc) me.mod_env p in
                if not (Ident.same ident (Path.head p)) then
                  self#zero
                else
                  let p =
                    if Ident.persistent (Path.head p) then
                      snd (Buildenv.Factorise.path p)
                    else p in
                  let p = Chain.of_path mod_sort p in
                  let frame = Modulescope.peek scope in
                  match Chain.drop p id_tail, frame with
                  | None, _
                    (* This should not happen since [id_tail] should refer to a
                       value and [p] to a module. *)
                  | _, Modulescope.Parameter _ ->
                    (* This should not happen since we (should) have come
                       directly from a module binding or an include declaration. *)
                    assert false
                  | Some ((Chain.Atomic _) as id_tail),
                    Modulescope.Module (None, _, _)
                      when
                        let open Modulescope in
                        let frame = peek scope in
                        frame_is_naked frame || in_frame_sigs id_tail frame ->
                    (* The rationale for treating this case separately is that
                       when a value is included, a new binding is created, but
                       the normalized path of included modules still contains
                       the prefix that was included. *)
                    let id_tail = Chain.lift id_tail in
                    let full_id =
                      Option.get_exn
                        (Modulescope.to_identifier ~id_tail scope) in
                    DepSet.singleton
                      (Repr.Rename ((input_lib (), full_id), to_id ()),
                        (me.mod_loc, Deps.ModuleInclude))
                  | Some id_tail, _ ->
                    let () = Logging.info @@ fun _f -> _f
                      ~tags:(of_loc me.mod_loc)
                      "Checking dependencies of %a with tail: @[<h>%a@]"
                      Modulescope.pp_frame (Modulescope.peek scope)
                      Chain.pp id_tail in
                    let id_tail = Chain.lift id_tail in
                    let full_id =
                      Modulescope.to_identifier
                        ~check_sigs:true ~id_tail scope in
                    let dep =
                      Option.map
                        (fun full_id ->
                          Repr.Rename ((input_lib (), full_id), to_id ()))
                        (full_id) in
                    let dep_desc =
                      match Modulescope.(frame_id (peek scope)) with
                      | None   -> Deps.ModuleInclude
                      | Some _ -> Deps.ModuleIsAliased in
                    let dep_src = (me.mod_loc, dep_desc) in
                    let dep =
                      Option.map (fun dep -> (dep, dep_src)) dep in
                    Option.map_or ~default:self#zero DepSet.singleton dep
                  | exception Invalid_argument _ ->
                    (* [p] is not a prefix of id_tail *)
                    self#zero
                end
            | Tmod_structure _struct ->
              (* N.B. This could be an anonymous module include, if we have
                      reached here via an include declaration. *)
              self#visit_tt_structure scope _struct
            | Tmod_unpack _ ->
              (* Unpacking of a first class module. *)
              self#zero
            | Tmod_functor (x, _, x_type, body) ->
              (* N.B. No need to recurse into parameter signatures here *)
              let (body, tys) = Module.unwrap_expr body in
              let param = (Option.map (Pair.make x) x_type, tys) in
              self#visit_tt_module_expr
                (Modulescope.push_param param scope)
                (body)
            | Tmod_apply _ ->
              (* TODO: Not yet handling functors - log a message? *)
              self#zero

          (* TODO: Non-local dependencies in signatures? *)
          (* E.g. sig
                    include module type of
                      struct
                        include M
                      end
                  end
            where M is a parent module of the value being renamed.

            Something like the following is OK, since the module equality will
            have to be witnessed in the implementation anyway:

                  sig
                    module N = M
                  end

            where M is a parent of the value being renamed.

          *)
          (* TODO: module "with" constraints will induce dependencies,
                   e.g. when renaming [id],

                      S with module [:]= [mod_id]

                   if [mod_id] is a prefix of [id].
          *)
          (* For now, just return the empty dependency set for signatures. *)
          method! visit_tt_signature env _sig = self#zero

        end

    end

    module Local = struct

      class ['a] env id scope dep_descr = object(self)
        inherit ['a] id_env id
        inherit module_scope_env scope
        inherit dep_descr_env dep_descr
      end

      let visitor =
        object(self)

          inherit [_] reduce_with_id as super
          inherit DepSet.monoid

          method! process_binding_from_struct env this
              (type a)
                ((id : (Ident.t, a) Atom.t), _)
                (((_, InStr (_, rest), InStr { str_env; str_loc; }, binding) as source)
                  : (a, impl) binding_source) =
            let err =
              lazy
                (invalid_arg
                  (Format.sprintf
                    "%s.Deps.Local.visitor#process_binding_from_struct"
                    __MODULE__)) in
            let locals =
              match id, binding with
              | Atom.StructureType _, _ ->
                Lazy.force err
              | Atom.FunctorType _, _ ->
                Lazy.force err
              | _, `Binding Value _ ->
                self#zero
              | _, `Binding Structure (_, InStr mb) ->
                self#visit_tt_module_binding env mb
              | _, `Binding Functor (_, InStr mb) ->
                self#visit_tt_module_binding env mb
              | _, `Include InStr decl ->
                self#visit_tt_include_declaration env decl in
            let nonlocals =
              match id with
              | Atom.Value _ ->
                self#zero
              | _ ->
                match Moduletype.find_lookup id (next_env source) env#id with
                | None ->
                  self#zero
                | Some _ ->
                  let Atom.Data.{ id; _ } = Atom.unwrap id in
                  (Nonlocal.visitor (id, env#id))#visit_tt_structure
                    env#scope
                    { this with
                        str_items =
                          let InStr rest = next_items source in rest } in
              self#plus locals nonlocals

          (* TODO: Do we need to visit the module types that form the scope? *)
          method! visit_tt_module_binding env mb =
            let id = Chain.tl_exn env#id in
            let scope, (InStr me) =
              Modulescope.enter_module_binding (InStr mb) env#scope in
            self#visit_tt_module_expr
              (new env id scope (Some Deps.ModuleIsAliased)) me
          method! visit_tt_include_declaration env decl =
            let scope, (InStr me) =
              Modulescope.enter_include (InStr decl) env#scope in
            let env = env#with_scope scope in
            let env = env#with_dep_descr Deps.ModuleInclude in
            self#visit_tt_module_expr env me

          method! visit_tt_module_expr env me =
            (* Do the match here rather than leaving it to the generic traversal
               since we need access to the location and environment of the
               module expression. *)
            let log_warning msg =
              Logging.warn @@ fun _f -> _f
                ~tags:(of_loc me.mod_loc)
                "module bound to %s - cannot do local renaming!" msg in
            match me.mod_desc with
            | Tmod_functor (x, _, x_type, body) ->
              self#visit_functor env x x_type (InStr body)
            | Tmod_apply _ ->
              (* TODO *)
              let () = log_warning "a functor application" in
              self#zero
            | Tmod_unpack _ ->
              let () = log_warning "an expression unpacking" in
              self#zero
            | Tmod_constraint _ ->
              (* Not expecting to get here since we have stripped off
                 constraints when traversing module scopes. *)
              assert false
            | Tmod_structure str ->
              self#visit_tt_structure env str
            | Tmod_ident (p, _) ->
              self#visit_modident
                env Impl (me.mod_loc, me.mod_env, me.mod_type) p

          method! process_binding_from_sig env this
              (type a)
                ((id : (Ident.t, a) Atom.t),
                  (typedata : a Types_views.item_element_view))
                ((_,_,_, binding) : (a, intf) binding_source) =
            let err =
              lazy
                (invalid_arg
                  (Format.sprintf
                    "%s.Deps.Local.visitor#process_binding_from_sig"
                    __MODULE__)) in
            match id, binding with
            | Atom.StructureType _, _ ->
              Lazy.force err
            | Atom.FunctorType _, _ ->
              Lazy.force err
            | _, `Binding Value _ ->
              self#zero
            | _, `Binding Structure (_, InSig md) ->
              self#visit_tt_module_declaration env md
            | _, `Binding Functor (_, InSig md) ->
              self#visit_tt_module_declaration env md
            | _, `Include InSig desc ->
              self#visit_tt_include_description env desc

          method! visit_tt_module_declaration env md =
            let scope, (InSig mt) =
              Modulescope.enter_module_binding (InSig md) env#scope in
            let id = Chain.tl_exn env#id in
            let env = new env id scope (Some Deps.InterfaceImplemented) in
            self#visit_tt_module_type env mt
          method! visit_tt_include_description env desc =
            let scope, (InSig mt) =
              Modulescope.enter_include (InSig desc) env#scope in
            let env = env#with_scope scope in
            let env = env#with_dep_descr Deps.SignatureInclude in
            self#visit_tt_module_type env mt

          (* N.B. We only get here as the type declared for a module binding. *)
          (*   Is this true? what about signature includes within such types? *)
          (*   This could also be the type of a functor argument.             *)
          method! visit_tt_module_type env mt =
            match mt.mty_desc with
            | Tmty_alias _ ->
              (* TODO: Find out what this means. Is this, e.g., the following:

                      ... sig
                            module M = N
                          end

                      So M has the module type alias N? *)
              assert false
            | Tmty_ident (p, _) ->
              (* module M : p
                   or
                 include p
                   So p = M_1.M_2. ... .S
                     where each M_i is a module, and S a module type *)
              (* We only generate a dependency if the value to be renamed is
                 declared a member of the module type. *)
              begin match
                  Moduletype.resolve_lookup mt.mty_env mt.mty_type env#id
              with
              | None ->
                self#zero
              | Some _ ->
                self#visit_modident
                  env Intf (mt.mty_loc, mt.mty_env, mt.mty_type) p
              end
            | Tmty_signature _sig ->
              self#visit_tt_signature env _sig
            | Tmty_functor (x, _, x_type, body) ->
              self#visit_functor env x x_type (InSig body)
            | Tmty_with (mty, _) ->
              (* TODO: Handle module "with" constraints *)
              self#visit_tt_module_type env mty
            | Tmty_typeof _ ->
              (* TODO: Something to be done here? e.g.

                   module M :
                     module type of
                       struct
                         include M'
                       end
                     = ...

                 where M' is a parent module of the value being renamed.  *)
              let () = Logging.warn @@ fun _f -> _f
                ~tags:(of_loc mt.mty_loc)
                "module type is type of a module expression - cannot generate \
                 local dependencies!" in
              self#zero

          method visit_modident
              : 'a . _ -> ('a Elements.Base.sort) -> _ -> _ -> _ =
            fun (env : Elements.Base._value env)
                (type a) (sort : a Elements.Base.sort)
                (mod_loc, mod_env, mod_type) mod_path ->
              let p = Env.normalize_path (Some mod_loc) mod_env mod_path in
              let head = Path.head p in
              let modtype_kind =
                match Moduletype.sort mod_env mod_type with
                | None ->
                  let () = Logging.err @@ fun _f -> _f
                    ~tags:(of_loc mod_loc)
                    "Could not determine the type of module %a!"
                    Printtyp.path p in
                  failwith
                    (Format.sprintf
                      "Could not determine the type of module %a!"
                      Printtyp.path p)
                | Some kind ->
                  kind in
              let id_kind =
                match sort with
                | Impl ->
                  Elements.(Module._lift (ModuleType.type_of modtype_kind))
                | Intf ->
                  Elements.ModuleType._lift modtype_kind in
              let lib, (Chain.Ex (_, parent)) =
                if Ident.persistent head then
                  Pair.map2
                    (Chain._of_path id_kind)
                    (Buildenv.Factorise.path p)
                else
                  let parent =
                    match Modulescope.find_param head env#scope with
                    | Some (_, parent_scope) ->
                      let id_tail =
                        Option.map
                          (Chain._of_longident id_kind)
                          (path_drop (Path.Pident head) p) in
                      Option.get_exn
                        Modulescope.(to_identifier
                          ?id_tail
                          (enter_param head parent_scope))
                    | None ->
                      let Atom.Ex head =
                        match p with
                        | Path.Pident _ ->
                          Atom._mk id_kind (Atom.Data.only head)
                        | Path.Pdot _ ->
                          Atom.(_mk (Ex Structure) (Atom.Data.only head))
                        | Path.Papply _ ->
                          assert false in
                      match find_path_to_local_binding head with
                      | None ->
                        failwith
                          (Format.sprintf "Could not find %a locally"
                            (Atom.pp Printtyp.ident) head)
                      | Some Chain.Ex (_, c) ->
                        Chain._append c (Chain._of_path id_kind p) in
                  input_lib (), parent in
              let id = Chain.append parent env#id in
              DepSet.singleton
                (Repr.Rename ((lib, (Chain.Ex (Value, id))), to_id ()),
                  (mod_loc, (Option.get_exn env#dep_descr)))

          method visit_functor
              : 'a . _ -> _ -> _ -> 'a Typedtree_views.module_expr_view -> _ =
            fun (env : Elements.Base._value env) x x_type
                (type a) (body : a Typedtree_views.module_expr_view) ->
              (* Note, we rely on the fact that we must have come through a
                 module binding/declaration to reuse the ModuleIsAliased or
                 InterfaceImplemented dependency descriptions respectively -
                 functors cannot be `include`d. *)
              match Chain.hd env#id with
              | Atom.Ex Atom.Parameter (_, None) ->
                (* We expect identifier construction to force functor parameters
                  to be indexed. *)
                assert false
              | Atom.Ex Atom.Parameter (_, Some idx)
                  when
                    idx = List.length (Modulescope.peek_params env#scope) + 1 ->
                begin match x_type with
                | None ->
                  failwith
                    (Format.sprintf
                      "%s.Deps.Local.visitor#visit_functor: cannot enter unit \
                       functor parameter"
                      __MODULE__)
                | Some x_type ->
                  let locals =
                    let id = Chain.tl_exn env#id in
                    let scope = Modulescope.enter_param x env#scope in
                    let env = env#with_id id in
                    let env = env#with_scope scope in
                    self#visit_tt_module_type env x_type in
                  let nonlocals =
                    match body with
                    | InSig _ ->
                      self#zero
                    | InStr body ->
                      (Nonlocal.visitor (x, env#id))#visit_tt_module_expr
                        env#scope
                        body in
                  self#plus locals nonlocals
                end
              | _ ->
                match body with
                | InStr body ->
                  let e, tys = Module.unwrap_expr body in
                  let param = Option.map (Pair.make x) x_type in
                  let scope = Modulescope.push_param (param, tys) env#scope in
                  self#visit_tt_module_expr (env#with_scope scope) e
                | InSig body ->
                  let param = (Option.map (Pair.make x) x_type, []) in
                  let env =
                    env#with_scope (Modulescope.push_param param env#scope) in
                  self#visit_tt_module_type env body

        end

    end

  end

  let get_deps_nonlocal cont =
    lookup_head_ident (Option.map_or ~default:DepSet.empty cont)

  let get_sig_deps local scope _sig =
    if local then
      let from_id = Chain.tl_exn (from_id ()) in
      Deps.Local.
        (visitor#visit_tt_signature (new env from_id scope None) _sig)
    else
      get_deps_nonlocal
        (fun ident ->
          let visitor = Deps.Nonlocal.visitor (ident, from_id ()) in
          visitor#visit_tt_signature scope _sig)

  let get_struct_deps local scope _struct =
    if local then
      let from_id = Chain.tl_exn (from_id ()) in
      Deps.Local.
        (visitor#visit_tt_structure (new env from_id scope None) _struct)
    else
      get_deps_nonlocal
        (fun ident ->
          let visitor = Deps.Nonlocal.visitor (ident, from_id ()) in
          visitor#visit_tt_structure scope _struct)

  let get_deps ~mli input =
    let infos = input.fileinfos in
    let local = is_local infos in
    let mod_scope =
      Option.map_or
        ~default:((module_name infos, None), []) initial_module_scope mli in
    let () = set_input input in
    let deps =
      dispatch_on_input_type
        ~intf_f:(get_sig_deps local mod_scope)
        ~impl_f:(get_struct_deps local mod_scope) in
    let () =
      if not (DepSet.is_empty deps) then
        Logging.info @@ fun _f -> _f
          ~header:"Deps:"
          "@[<v>%a@]" (DepSet.pp Refactoring.Deps.Elt.pp) deps in
    deps

end