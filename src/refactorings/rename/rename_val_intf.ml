open Containers
open Fun

open Compiler

open Typedtree
open Asttypes
open Ident

open Refactoring
open Refactoring_utils
open Refactoring_visitors
open Rename_core

open Sourcefile
open Fileinfos

(* open Elements *)
open Elements.Base
open Typedtree_views
open Identifier

open Lib
open Ast_lib
open Logging
open Tags

open Ast_utils
open Compiler_utils

module DepSet = Deps.Elt.Set

module Make () = struct

  include InputState.Make ()
  include ParameterState.Make (struct
      type kind_t = _value
      let kind = Elements.Base.Value
      let variety = Chain.InIntf
    end)

  let name = "Rename interface value"

  (* ------------------------------------------------------------------------
      REPLACEMENTS
    ------------------------------------------------------------------------- *)

  class ['a] replacement_env id scope = object(self)
    inherit ['a] id_env id
    inherit module_scope_env scope
  end

  let check_soundness env mt =
    let to_id = Chain.mk (Atom.mk Value (Atom.Data.only (to_id ()))) in
    if Option.is_some (Moduletype.lookup env mt to_id) then
      let err =
        SoundnessViolation
          (Format.sprintf
            "Module already contains a binding for %a" Chain.pp to_id) in
      raise (Error (err, None))

  let replace_local =
    object (self)

      inherit [_] reduce_with_id as super
      inherit Replacement.Set.monoid

      method! process_binding_from_struct env this
          (type b)
            ((id : (Ident.t, b) Atom.t), _)
            ((_,_,_, binding) : (b, impl) binding_source) =
        let open Atom in
        match id, binding with
        | Value _, _ ->
          invalid_arg
            (Format.sprintf
              "%s.replace_local#process_binding_from_struct"
              __MODULE__)
        | _, `Binding Structure (_, InStr mb) ->
          self#visit_tt_module_binding env mb
        | _, `Binding StructureType InStr mtd ->
          self#visit_tt_module_type_declaration env mtd
        | _, `Binding Functor (_, InStr mb) ->
          self#visit_tt_module_binding env mb
        | _, `Binding FunctorType InStr mtd ->
          self#visit_tt_module_type_declaration env mtd
        | _, `Include InStr incl ->
            self#visit_tt_include_declaration env incl

      method! visit_tt_module_binding env mb =
        let id = Chain.tl_exn env#id in
        let scope, _ =
          Modulescope.enter_module_binding (InStr mb) env#scope in
        super#visit_tt_module_binding (new replacement_env id scope) mb
      method! visit_tt_include_declaration env decl =
        let scope, _ = Modulescope.enter_include (InStr decl) env#scope in
        super#visit_tt_include_declaration (env#with_scope scope) decl

      method! visit_tt_module_expr env this =
        match this.mod_desc with
        | Tmod_functor (x, _, x_type, body) ->
          self#visit_functor env x x_type (InStr body)
        | Tmod_apply _ ->
          let () = Logging.warn @@ fun _f -> _f
            ~tags:(of_loc this.mod_loc)
            "module bound to a functor application - cannot do local renaming!" in
          self#zero
        | Tmod_unpack _ ->
          let () = Logging.warn @@ fun _f -> _f
            ~tags:(of_loc this.mod_loc)
            "module bound to an expression unpacking - cannot do local renaming!" in
          self#zero
        | _ ->
          super#visit_tt_module_expr env this

      method! process_binding_from_sig env this
          (type b)
            ((id : (Ident.t, b) Atom.t), _)
            ((_,_, InSig { sig_env; _ }, binding) : (b, intf) binding_source) =
        let sound =
          lazy (check_soundness sig_env (Types_views.ST this.sig_type)) in
        let open Atom in
        match id, binding with
        | _, `Binding Value (_, InSig { val_name = { loc; _}; _ }) ->
          let () = Lazy.force sound in
          Replacement.Set.of_opt (Replacement.mk_opt loc (to_id ()))
        | Value _, `Include InSig { incl_mod; _ } ->
          (* This should handle anonymous signature includes *)
          let () = Lazy.force sound in
          self#visit_tt_module_type env incl_mod
        | _, `Binding Structure (_, InSig md) ->
          self#visit_tt_module_declaration env md
        | _, `Binding StructureType InSig mtd ->
          self#visit_tt_module_type_declaration env mtd
        | _, `Binding Functor (_, InSig md) ->
          self#visit_tt_module_declaration env md
        | _, `Binding FunctorType InSig mtd ->
          self#visit_tt_module_type_declaration env mtd
        | _, `Include InSig incl ->
          self#visit_tt_include_description env incl

      method! visit_tt_module_declaration env md =
        let id = Chain.tl_exn env#id in
        let scope, _ =
          Modulescope.enter_module_binding (InSig md) env#scope in
        super#visit_tt_module_declaration (new replacement_env id scope) md

      method! visit_tt_include_description env desc =
        let scope, _ = Modulescope.enter_include (InSig desc) env#scope in
        super#visit_tt_include_description (env#with_scope scope) desc

      method! visit_tt_module_type env this =
        match this.mty_desc with
        | Tmty_functor (x, _, x_type, body) ->
          self#visit_functor env x x_type (InSig body)
        | Tmty_typeof _ ->
          let () = Logging.warn @@ fun _f -> _f
            ~tags:(of_loc this.mty_loc)
            "module has type of a module expression - cannot do local renaming!" in
          self#zero
        | _ ->
          super#visit_tt_module_type env this

      method visit_functor
          : 'a . _ -> _ -> _ -> 'a Typedtree_views.module_expr_view -> _ =
        fun env x x_type
            (type a) (body : a Typedtree_views.module_expr_view) ->
          let num_params =
            List.length (Modulescope.peek_params env#scope) + 1 in
          match Chain.hd env#id with
          | Atom.Ex Atom.Parameter (_, None) ->
            (* We expect identifier construction to force functor
                parameters to be indexed. *)
            assert false
          | Atom.Ex Atom.Parameter (_, Some idx) when idx = num_params ->
            begin match x_type with
            | None ->
              failwith
                (Format.sprintf
                  "%s.Reps.Local.visitor#visit_functor: cannot enter \
                    unit functor parameter"
                  __MODULE__)
            | Some x_type ->
              let id = Chain.tl_exn env#id in
              let scope = Modulescope.enter_param x env#scope in
              self#visit_tt_module_type (new replacement_env id scope) x_type
            end
          | _ ->
            match body with
            | InStr body ->
              let _, tys = Module.unwrap_expr body in
              let param = Option.map (Pair.make x) x_type in
              let scope = Modulescope.push_param (param, tys) env#scope in
              self#visit_tt_module_expr (env#with_scope scope) body
            | InSig body ->
              let param = (Option.map (Pair.make x) x_type, []) in
              let env =
                env#with_scope (Modulescope.push_param param env#scope) in
              self#visit_tt_module_type env body

    end

  (* N.B. No need for non-local replacements, since the value in the interface
          can never be referred to non-locally in a direct manner; only the
          top-level parent module type can be referred to non-locally. *)

  let process_sig scope _sig =
    replace_local#visit_tt_signature
      (new replacement_env (Chain.tl_exn (from_id ())) scope) _sig

  let process_struct scope _struct =
    let replacements =
      replace_local#visit_tt_structure
        (new replacement_env (Chain.tl_exn (from_id ())) scope) _struct in
    let () = if Replacement.Set.is_empty replacements then
      (* TODO: Consider: do we need to emit this warning? *)
      Logging.warn @@ fun _f -> _f
        "Could not find local value to rename in %s!"
        (get_input ()).fileinfos.filename in
    replacements

  let process_file input =
    let () = set_input input in
    let infos = input.fileinfos in
    if is_local infos then
      let mod_scope = ((module_name infos, None), []) in
      try
        dispatch_on_input_type
          ~intf_f:(process_sig mod_scope)
          ~impl_f:(process_struct mod_scope)
      (* TODO: Catch this exception somewhere more appropriate *)
      with Env.Error _ as e ->
        let e = error_of_exn e in
        let backtrace = get_raw_backtrace () in
        raise (Error (CompilerError e, backtrace))
    else
      Replacement.Set.empty

  (* ------------------------------------------------------------------------
      DEPENDENCIES
     ------------------------------------------------------------------------ *)

  module Deps = struct

    module NonLocal = struct

      let visitor (ident, id_tail) =
        object (self)

          inherit [_] dep_reducer as super

          (* This will control the direction of recursion through structures.
             For now, we want to rule out recursing into certain structure
             items (e.g. values/evals) in order to avoid generating bogus
             dependencies. *)
          (* TODO: work out what (if anything) needs doing in different kinds of
                   structure items. *)
          method! visit_tt_structure_item_desc scope this =
            match this with
            | Tstr_eval _
            | Tstr_value _
            | Tstr_primitive _
            | Tstr_type _
            | Tstr_typext _
            | Tstr_exception _
            | Tstr_open _
            | Tstr_class _
            | Tstr_class_type _
            | Tstr_attribute _
              ->
              self#zero
            | Tstr_module _
            | Tstr_recmodule _
            | Tstr_modtype _
            | Tstr_include _
              ->
              super#visit_tt_structure_item_desc scope this

            method! visit_tt_module_binding scope mb =
            let scope, (InStr me) =
              Modulescope.enter_module_binding (InStr mb) scope in
            self#visit_tt_module_expr scope me

          method! visit_tt_include_declaration scope decl =
            let scope, (InStr me) =
              Modulescope.enter_include (InStr decl) scope in
            self#visit_tt_module_expr scope me

          method! visit_tt_module_expr scope me =
            match me.mod_desc with
            | Tmod_constraint _ ->
              (* Shouldn't get here as the [Modulescope.enter_] functions should
                 have stripped all the constraints off before recursing. *)
              assert false
            | Tmod_unpack _ ->
              self#zero
            | Tmod_ident _ ->
              (* TODO: Here we should catch module aliases, i.e.

                    module M = M_1. ... .M_n

                 where M_1. ... .M_n is a prefix of [parent_mt] *)
              self#zero
            | Tmod_structure _struct ->
              self#visit_tt_structure scope _struct
            | Tmod_functor (x, _, x_type, body) ->
              self#visit_functor scope x x_type (InStr body)
            | Tmod_apply _ ->
              (* TODO: Log a warning *)
              self#zero

          (* TODO: The following two methods may need modifying to detect whether
                   they should add a structure(type) or functor(type) scope frame. *)

          method! visit_tt_module_declaration scope md =
            let scope, (InSig mt) =
              Modulescope.enter_module_binding (InSig md) scope in
            self#visit_tt_module_type scope mt

          method! visit_tt_module_type_declaration scope mtd =
            let scope, mt =
              Modulescope.enter_module_type_declaration mtd scope in
            self#visit_option self#visit_tt_module_type scope mt

          (* N.B. includes in module types (include descriptions) just handled
                  by the recursion fall-through: no extra scope needs to be
                  added. *)

          method! visit_tt_module_type scope mty =
            let mty, constraints = Moduletype.unwrap mty in
            let constraints = List.flatten constraints in
            match mty.mty_desc with
            | Tmty_with _ ->
              (* We should have stripped off all the constraints *)
              assert false
            | Tmty_ident (p, _) ->
              let mty_sort =
                if not (List.is_empty constraints) then
                  Some (Elements.Base.(Ex Structure))
                else
                  Option.map
                    Elements.ModuleType._lift
                    (Moduletype.sort mty.mty_env mty.mty_type) in
              begin match mty_sort with
              | None ->
                let () = Logging.info @@ fun _f -> _f
                  ~tags:(of_loc mty.mty_loc)
                  "Could not determine sort of module type expression: \
                   cannot check nonlocal dependencies" in
                self#zero
              | Some Elements.Base.Ex mty_sort ->
                let p = Env.normalize_path (Some mty.mty_loc) mty.mty_env p in
                if not (Ident.same ident (Path.head p)) then
                  self#zero
                else
                  let p =
                    if Ident.persistent (Path.head p) then
                      snd (Buildenv.Factorise.path p)
                    else p in
                  let p = Chain.of_path mty_sort p in
                  match Chain.drop p id_tail with
                  | None ->
                    assert false
                  | Some id_tail ->
                    let Atom.Ex hd = Chain.hd id_tail in
                    let Atom.Data.{ id = m'; _ } = Atom.unwrap hd in
                    begin match
                      List.find_map
                        (function
                          | _, { txt = Longident.Lident m; _ },
                            Twith_modsubst (p, { loc; _ })
                              when m = m' ->
                            Some (p, loc)
                          | _ ->
                            None)
                        (constraints)
                    with
                    | None ->
                      let id_tail = Chain.lift id_tail in
                      let full_id =
                        Option.get_exn
                          (Modulescope.to_identifier ~id_tail scope) in
                      (* TODO: This could come from a signature include! *)
                      DepSet.singleton
                        (Repr.Rename ((input_lib (), full_id), (to_id ())),
                          (mty.mty_loc, Deps.InterfaceImplemented))
                    | Some (p, loc) ->
                      let p =
                        Env.normalize_path (Some mty.mty_loc) mty.mty_env p in
                      let mod_kind =
                        match Env.find_module p mty.mty_env with
                        | Types.{ md_type; _ } ->
                          Option.map
                            Elements.(Module._lift % ModuleType.type_of)
                            (Moduletype.sort mty.mty_env md_type)
                        | exception Not_found ->
                          None in
                      let mod_kind =
                        mod_kind |>
                          Option.get_lazy @@ fun () ->
                            let () = Logging.err @@ fun _f -> _f
                              ~tags:(of_loc loc)
                              "Could not determine sort of module path %a in \
                              module type constraint."
                              Printtyp.path p in
                            failwith
                              (Format.sprintf
                                "Could not determine sort of module %a"
                                Printtyp.path p) in
                      let head = Path.head p in
                      let lib, (Chain.Ex (_, parent)) =
                        if Ident.persistent head then
                          Pair.map2
                            (Chain._of_path (Ex Structure))
                            (Buildenv.Factorise.path p)
                        else
                          let parent =
                            match Modulescope.find_param head scope with
                            | Some (_, parent_scope) ->
                              let id_tail =
                                Option.map
                                  (Chain._of_longident mod_kind)
                                  (path_drop (Path.Pident head) p) in
                              Option.get_exn
                                Modulescope.(to_identifier
                                  ?id_tail
                                  (enter_param head parent_scope))
                            | None ->
                              let Atom.Ex head =
                                match p with
                                | Path.Pident _ ->
                                  Atom._mk mod_kind (Atom.Data.only head)
                                | Path.Pdot _ ->
                                  Atom._mk (Ex Structure) (Atom.Data.only head)
                                | Path.Papply _ ->
                                  assert false in
                              match find_path_to_local_binding head with
                              | None ->
                                failwith
                                  (Format.sprintf
                                    "Could not find module %a locally"
                                    (Atom.pp Printtyp.ident) head)
                              | Some Chain.Ex (_, c) ->
                                Chain._append c (Chain._of_path mod_kind p) in
                          input_lib (), parent in
                      let id = Chain.append parent id_tail
                        in
                      DepSet.singleton
                        (Repr.Rename ((lib, (Chain.Ex (Value, id))), to_id ()),
                          (mty.mty_loc, Deps.ModuleSubstitutedInSignature))
                    end
                  | exception Invalid_argument _ ->
                    (* [p] is not a prefix of id_tail *)
                    self#zero
              end
            | Tmty_signature _sig ->
              self#visit_tt_signature scope _sig
            | Tmty_functor (x, _, x_type, body) ->
              self#visit_functor scope x x_type (InSig body)
            | Tmty_typeof _ ->
              let () = Logging.warn @@ fun _f -> _f
                ~tags:(of_loc mty.mty_loc)
                "module type is type of a module expression - \
                 cannot generate nonlocal dependencies!" in
              self#zero
            | _ ->
              (* TODO: For Tmty_alias, check whether the alias is a prefix of
                       [parent_mt] *)
              self#zero

          method visit_functor :
              'a . _ -> _ -> _ -> 'a Typedtree_views.module_expr_view -> _
            = fun (type a)
                scope x x_type (body : a Typedtree_views.module_expr_view) ->
            let arg_deps =
              self#visit_option self#visit_tt_module_type
                (Modulescope.enter_param x scope)
                (x_type) in
            let body_deps =
              match body with
              | InStr body ->
                let (body, tys) = Module.unwrap_expr body in
                let param = (Option.map (Pair.make x) x_type, tys) in
                self#visit_tt_module_expr
                  (Modulescope.push_param param scope)
                  (body)
              | InSig body ->
                let param = (Option.map (Pair.make x) x_type, []) in
                self#visit_tt_module_type
                  (Modulescope.push_param param scope)
                  (body) in
            self#plus arg_deps body_deps

        end

      (* let get env =
        let mty, tail =
          Pair.map2 Option.get_exn (Chain.split_at_interface (from_id ())) in
        match mty with
        | Chain.FT _ ->
          invalid_arg (Format.sprintf "%s.Deps.NonLocal.get" __MODULE__)
        | Chain.ST mty ->
          let mty =
            lookup_path
              ~lookup_f:(fun lid -> fst (Env.lookup_modtype lid env))
              ?backup_lib:((get_input ()).fileinfos.library)
              ~env
              (from_lib (), Chain.to_longident mty) in
          Option.map ((Fun.flip mk_reducer) tail) mty *)

    end

    module Local = struct

      class ['a] env id scope dep_descr = object (self)
        inherit ['a] id_env id
        inherit module_scope_env scope
        inherit dep_descr_env dep_descr
      end

      let visitor =
        object (self)

          inherit [_] reduce_with_id as super
          inherit DepSet.monoid

          method! process_binding_from_struct env this
              (type a)
                ((id : (Ident.t, a) Atom.t), _)
                (((_, _, _, binding) as source) : (a, impl) binding_source) =
            let locals =
              match id, binding with
              | Atom.Value _, _ ->
                invalid_arg
                  (Format.sprintf
                    "%s.Deps.Local.visitor#process_binding_from_struct: \
                    match case [_ (%a)]"
                    __MODULE__
                    Elements.Base.pp (Atom.kind id))
              | _, `Binding Structure (_, InStr mb) ->
                self#visit_tt_module_binding env mb
              | _, `Binding StructureType InStr decl ->
                self#visit_tt_module_type_declaration env decl
              | _, `Binding Functor (_, InStr mb) ->
                self#visit_tt_module_binding env mb
              | _, `Binding FunctorType InStr decl ->
                self#visit_tt_module_type_declaration env decl
              | _, `Include InStr decl ->
                self#visit_tt_include_declaration env decl in
            let Atom.Data.{ id; _ } = Atom.unwrap id in
            let nonlocal = NonLocal.visitor (id, env#id) in
            let rest =
              { this with
                  str_items =
                    let InStr rest = next_items source in rest } in
            let non_locals = nonlocal#visit_tt_structure env#scope rest in
            self#plus locals non_locals

          method! visit_tt_module_binding env mb =
            let scope, _ =
              Modulescope.enter_module_binding (InStr mb) env#scope in
            let id = Chain.tl_exn env#id in
            let env = new env id scope (Some Deps.ModuleIsAliased) in
            super#visit_tt_module_binding env mb

          method! visit_tt_include_declaration env decl =
            let scope, _ =
              Modulescope.enter_include (InStr decl) env#scope in
            let env = new env env#id scope (Some Deps.ModuleInclude) in
            super#visit_tt_include_declaration env decl

          method! visit_tt_module_type_declaration env mtd =
            let scope, mty =
              Modulescope.enter_module_type_declaration mtd env#scope in
            let id = Chain.tl_exn env#id in
            let env = new env id scope (Some Deps.SignatureIsAliased) in
            match Chain.variety env#id with
            | Chain.InImpl ->
              (* Mustn't recurse if we have consumed the head interface. *)
              self#zero
            | Chain.InIntf ->
              super#visit_tt_module_type_declaration env mtd

          method! visit_tt_module_expr env me =
            (* Do the match here rather than leaving it to the generic traversal
              since we need access to the location and environment of the module
              expression. *)
            let log_warning msg =
              Logging.warn @@ fun _f -> _f
                ~tags:(of_loc me.mod_loc)
                "module bound to %s - cannot do local renaming!" msg in
            match me.mod_desc with
            | Tmod_functor (x, _, x_type, body) ->
              self#visit_functor env x x_type (InStr body)
            | Tmod_apply _ ->
              let () = log_warning "a functor application" in
              self#zero
            | Tmod_unpack _ ->
              let () = log_warning "an expression unpacking" in
              self#zero
            | Tmod_constraint _ ->
              (* Not expecting to get here since we have stripped off constraints
                 when traversing module scopes. *)
              assert false
            | Tmod_structure str ->
              self#visit_tt_structure env str
            | Tmod_ident (id, _) ->
              let id = Env.normalize_path (Some me.mod_loc) me.mod_env id in
              (* Determine if id refers to a local module or not, and turn the id
                 into a valid long identifier. *)
              let mod_kind =
                Option.map
                  Elements.(Module._lift % ModuleType.type_of)
                  (Moduletype.sort me.mod_env me.mod_type) in
              let mod_kind =
                mod_kind |>
                  Option.get_lazy @@ fun () ->
                    let () = Logging.err @@ fun _f -> _f
                      ~tags:(of_loc me.mod_loc)
                      "Could not determine sort of module path %a in \
                      module type constraint."
                      Printtyp.path id in
                    failwith
                      (Format.sprintf
                        "Could not determine sort of module %a"
                        Printtyp.path id) in
              let head = Path.head id in
              let lib, Chain.Ex (_, parent) =
                if Ident.persistent head then
                  Pair.map2
                    (Chain._of_path mod_kind)
                    (Buildenv.Factorise.path id)
                else
                  let parent =
                    match Modulescope.find_param head env#scope with
                    | Some (_, parent_scope) ->
                      let id_tail =
                        Option.map
                          (Chain._of_longident mod_kind)
                          (path_drop (Path.Pident head) id) in
                      Option.get_exn
                        Modulescope.(to_identifier
                          ?id_tail
                          (enter_param head parent_scope))
                    | None ->
                      let Atom.Ex m = Atom._mk mod_kind (Atom.Data.only head) in
                      match find_path_to_local_binding m with
                      | None ->
                        failwith
                          (Format.sprintf
                            "Could not find module %a locally"
                            Printtyp.ident head)
                      | Some Chain.Ex (_, c) ->
                        Chain._append c (Chain._of_path mod_kind id) in
                  input_lib (), parent in
              let id = Chain.append parent env#id in
              DepSet.singleton
                (Repr.Rename ((lib, (Chain.Ex (Value, id))), to_id ()),
                 (me.mod_loc, (Option.get_exn env#dep_descr)))

          method! process_binding_from_sig env this
              (type a)
                ((id : (Ident.t, a) Atom.t), _)
                (((_, _, _, binding) as source) : (a, intf) binding_source) =
            let locals =
                match binding with
              | `Binding Value _ ->
                self#zero
              | `Binding Structure (_, InSig md) ->
                self#visit_tt_module_declaration env md
              | `Binding Functor (_, InSig md) ->
                self#visit_tt_module_declaration env md
              | `Binding StructureType InSig mtd ->
                self#visit_tt_module_type_declaration env mtd
              | `Binding FunctorType InSig mtd ->
                self#visit_tt_module_type_declaration env mtd
              | `Include InSig desc ->
                self#visit_tt_include_description env desc in
            let Atom.Data.{ id; _ } = Atom.unwrap id in
            let nonlocal = NonLocal.visitor (id, env#id) in
            let rest =
              { this with
                  sig_items =
                    let InSig rest = next_items source in rest } in
            let non_locals = nonlocal#visit_tt_signature env#scope rest in
            self#plus locals non_locals

          method! visit_tt_module_declaration env md =
            let scope, (InSig mty) =
              Modulescope.enter_module_binding (InSig md) env#scope in
            let id = Chain.tl_exn env#id in
            let env = new env id scope (Some Deps.InterfaceImplemented) in
            super#visit_tt_module_type env mty

          method! visit_tt_include_description env incl =
            let scope, (InSig mty) =
              Modulescope.enter_include (InSig incl) env#scope in
            let env = new env env#id scope (Some Deps.SignatureInclude) in
            super#visit_tt_module_type env mty

          method! visit_tt_module_type env mt =
            let process_ident (type a) (p, (sort : a Elements.Base.sort)) =
              match Moduletype.resolve_lookup mt.mty_env mt.mty_type env#id with
              | None ->
                self#zero
              | Some _ ->
                let p = Env.normalize_path (Some mt.mty_loc) mt.mty_env p in
                let kind = Moduletype.sort mt.mty_env mt.mty_type in
                let kind =
                  match sort with
                  | Intf ->
                    Option.map Elements.ModuleType._lift kind
                  | Impl ->
                    Option.map
                      Elements.(Module._lift % ModuleType.type_of)
                      kind in
                let kind =
                  kind |>
                    Option.get_lazy @@ fun () ->
                      let () = Logging.err @@ fun _f -> _f
                        ~tags:(of_loc mt.mty_loc)
                        "Could not determine sort of module path %a in \
                        module type constraint."
                        Printtyp.path p in
                      failwith
                        (Format.sprintf
                          "Could not determine sort of module %a"
                          Printtyp.path p) in
                let head = Path.head p in
                let lib, Chain.Ex (_, parent) =
                  if Ident.persistent head then
                    Pair.map2 (Chain._of_path kind) (Buildenv.Factorise.path p)
                  else
                    let parent =
                      match Modulescope.find_param head env#scope with
                      | Some (_, parent_scope) ->
                        let id_tail =
                          Option.map
                            (Chain._of_longident kind)
                            (path_drop (Path.Pident head) p) in
                        Option.get_exn
                          Modulescope.(to_identifier
                            ?id_tail
                            (enter_param head parent_scope))
                      | None ->
                        let Atom.Ex head =
                          match p with
                          | Path.Pident _ ->
                            Atom._mk kind (Atom.Data.only head)
                          | Path.Pdot _ ->
                            Atom._mk (Ex Structure) (Atom.Data.only head)
                          | Path.Papply _ ->
                            assert false in
                        match find_path_to_local_binding head with
                        | None ->
                          failwith
                            (Format.sprintf
                              "Could not find %a locally"
                              (Atom.pp Printtyp.ident) head)
                        | Some Chain.Ex (_, c) ->
                          Chain._append c (Chain._of_path kind p) in
                    input_lib (), parent in
                let id = Chain.append parent env#id in
                DepSet.singleton
                  (Repr.Rename ((lib, (Chain.Ex (Value, id))), to_id ()),
                    (mt.mty_loc, (Option.get_exn env#dep_descr))) in
            match mt.mty_desc with
            | Tmty_functor (x, _, x_type, body) ->
              self#visit_functor env x x_type (InSig body)
            | Tmty_typeof _ ->
              (* TODO: figure out what to do here *)
              let () = Logging.warn @@ fun _f -> _f
                ~tags:(of_loc mt.mty_loc)
                "module type is type of a module expression - cannot generate \
                 local dependencies!" in
              self#zero
            | Tmty_signature _sig ->
              self#visit_tt_signature env _sig
            | Tmty_with (mty, _) ->
              (* TODO: Deal with destructive module substitutions here. *)
              self#visit_tt_module_type env mty
            | Tmty_ident (p, _) ->
              process_ident (p, Elements.Base.Intf)
            | Tmty_alias (p, _) ->
              let () =
                Logging.debug @@ fun _f -> _f
                  "Processing alias %a at %a"
                    Printtyp.path p
                    Location.print_compact mt.mty_loc in
              process_ident (p, Elements.Base.Impl)

          method visit_functor
              : 'a . _ -> _ -> _ -> 'a Typedtree_views.module_expr_view -> _ =
            fun (env : Elements.Base._value env) x x_type
                (type a) (body : a Typedtree_views.module_expr_view) ->
              begin match Chain.hd env#id with
              | Atom.(Ex (Parameter (_, None))) ->
                (* We expect identifier construction to force functor parameters
                   to be indexed. *)
                   assert false
              | Atom.(Ex (Parameter (_, Some idx)))
                  when
                    idx = List.length (Modulescope.peek_params env#scope) + 1 ->
                begin match x_type with
                | None ->
                  failwith
                    (Format.sprintf
                      "%s.Deps.Local.visitor#visit_tt_module_expr: _
                        cannot enter unit functor parameter"
                      __MODULE__)
                | Some x_type ->
                  let locals =
                    let env =
                      env#with_scope (Modulescope.enter_param x env#scope) in
                    let env = env#with_id (Chain.tl_exn env#id) in
                    self#visit_tt_module_type env x_type in
                  let nonlocals =
                    let visitor = NonLocal.visitor (x, env#id) in
                    match body with
                    | InSig body ->
                      visitor#visit_tt_module_type env#scope body
                    | InStr body ->
                      visitor#visit_tt_module_expr env#scope body in
                  self#plus locals nonlocals
                end
              | _ ->
                match body with
                | InStr body ->
                  let (body, tys) = Module.unwrap_expr body in
                  let param = (Option.map (Pair.make x) x_type, tys) in
                  let env =
                    env#with_scope (Modulescope.push_param param env#scope) in
                  self#visit_tt_module_expr env body
                | InSig body ->
                  let param = (Option.map (Pair.make x) x_type, []) in
                  let env =
                    env#with_scope (Modulescope.push_param param env#scope) in
                  self#visit_tt_module_type env body
              end

        end

    end

  end

  let lookup_head_ident cont =
    let env = Env.empty in
    let Atom.Ex hd = Chain.hd (from_id ()) in
    let path =
      lookup_path
        ~lookup_f:(fun lid -> Env.lookup_module ~load:true lid env)
        ?backup_lib:((get_input ()).fileinfos.library)
        ~env
        (from_lib (), Longident.Lident Atom.((unwrap hd).Data.id)) in
    match path with
    | None ->
      cont None
    | Some (Path.Pident hd) ->
      cont (Some hd)
    | _ ->
      assert false

  let init_scope () =
    let input = get_input () in
    let infos = input.fileinfos in
    ((module_name infos, None), [])

  let init_local_env () =
    new Deps.Local.env (Chain.tl_exn (from_id ())) (init_scope ()) None

  let get_deps_nonlocal cont =
    lookup_head_ident (Option.map_or ~default:DepSet.empty cont)

  let get_sig_deps local scope _sig =
    if local then
      Deps.Local.visitor#visit_tt_signature (init_local_env ()) _sig
    else
      get_deps_nonlocal
        (fun ident ->
          let visitor = Deps.NonLocal.visitor (ident, from_id ()) in
          visitor#visit_tt_signature scope _sig)

  let get_struct_deps local scope _struct =
    if local then
      Deps.Local.visitor#visit_tt_structure (init_local_env ()) _struct
    else
      get_deps_nonlocal
        (fun ident ->
          let visitor = Deps.NonLocal.visitor (ident, from_id ()) in
          visitor#visit_tt_structure scope _struct)


  let get_deps ~mli input =
    let () = set_input input in
    let infos = input.fileinfos in
    let local = is_local infos in
    let scope =
      Option.map_or
        ~default:((module_name infos, None), []) initial_module_scope mli in
    let deps =
      dispatch_on_input_type
        ~intf_f:(get_sig_deps local scope)
        ~impl_f:(get_struct_deps local scope) in
    let () =
      if not (DepSet.is_empty deps) then
        Logging.info @@ fun _f -> _f
          ~header:"Deps:"
          "@[<v>%a@]" (DepSet.pp Refactoring.Deps.Elt.pp) deps in
    deps

end