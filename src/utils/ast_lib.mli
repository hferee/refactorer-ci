open Containers

open Compiler
open Asttypes

val location_to_string : Location.t -> string
val location_to_string_compact : Location.t -> string
val longident_to_string : Longident.t -> string
val path_to_string : Path.t -> string

val cmp_loc : Location.t -> Location.t -> int

val cmp_by_loc : Typedtree.expression -> Typedtree.expression -> int

val is_arg_pun : string -> arg_label * Typedtree.expression -> bool

val is_field_pun :
  string
    -> Types.label_description * (Longident.t loc * Typedtree.expression)
    -> bool

val longident_hd : Longident.t -> string
val longident_tl : Longident.t -> Longident.t
val build_longident : string list -> Longident.t
val longident_append : Longident.t -> Longident.t -> Longident.t

val path_drop : Path.t -> Path.t -> Longident.t option
val path_dest_ident : Path.t -> Ident.t
val path_to_longident : Path.t -> Longident.t