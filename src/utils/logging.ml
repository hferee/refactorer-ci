open Containers
open Format
open Fun

open Compiler

open Logs

open Lib
open Lib.ApplyTokens

type t = Logs.src

let default_level = None

let log_file = ref None
let log_level = ref default_level

let show_progress = ref false

let log = Src.create "refactoring.log"

include (val src_log log : LOG)

module TagDefs = struct
  let loc =
    Tag.def ~doc:"A location in a source file"
       "location" Location.print_loc
  let preamble =
    Tag.def ~doc:"A formatter used to print a preamble to the log entry"
      "preamble" (fun f fmt -> fmt f ())
end

module Tags = struct
  open TagDefs
  let mk def t = Tag.add def t Tag.empty
  let of_loc = mk loc
  let preamble s = mk preamble (fun fmt () -> Format.fprintf fmt "%s" s)
end

let reporter ?delegate () =
  let report src level ~over cont msgf =
    let cont _ = over (); cont () in
    let do_log f =
      msgf @@ fun ?header ?tags fmt ->
      let find_in_tags def =
        Option.flat_map (fun tags -> Tag.find def tags) tags in
      let loc = find_in_tags TagDefs.loc in
      let preamble = find_in_tags TagDefs.preamble in
      kfprintf cont f ("@[<v 4>[%a]%a%a%a @[<v>" ^^ fmt ^^ "@]@]@.")
        pp_level level
        (some (string |> (within |> brackets))) header
        (some (Tag.printer TagDefs.loc |> (within |> brackets))) loc
        (some (Tag.printer TagDefs.preamble |> after_char ' ')) preamble in
    if not (Src.equal src log) then
      let delegate = Option.get_or ~default:(reporter ()) delegate in
      delegate.report src level ~over cont msgf
    else if Option.is_none !log_file then
      do_log Format.err_formatter
    else
      let log_file = Option.get_exn !log_file in
      try
        IO.with_out_a log_file @@ fun log_file ->
        let f =
          make_formatter
            (Pervasives.output log_file)
            (fun () -> Pervasives.flush log_file) in
        do_log f
      with Sys_error s ->
        let () =
          prerr_endline (sprintf "Error writing to log file: %s" s) in
        cont () in
  { report }

let output_progress (msgf : ('a, unit) Logs.msgf) =
  if !show_progress then
    let () =
      msgf @@ fun ?header ?tags fmt -> fprintf Format.err_formatter fmt in
    flush err_formatter ()

let progress_statement ?(log=false) msgf =
  output_progress msgf ;
  if log then info msgf

let output_progress msgf default =
  match msgf with
  | None ->
    output_progress default
  | Some msgf ->
    let () = info msgf in
    output_progress
      (fun _f -> msgf (fun ?header ?tags fmt -> _f ?header ?tags (fmt ^^ "@,")))

let minor_progress ?msgf () =
  output_progress msgf (fun _f -> _f ".")

let major_progress ?msgf () =
  output_progress msgf (fun _f -> _f " done@,")

let level_opts = [
    "None",    None
  ; "App",     Some Logs.App
  ; "Error",   Some Logs.Error
  ; "Warning", Some Logs.Warning
  ; "Info",    Some Logs.Info
  ; "Debug",   Some Logs.Debug
  ]

let set_level l =
  log_level :=
    try
      List.assoc l level_opts
    with
      Not_found -> None

let speclist = [
    "-log-file",
      Arg.String
        (fun s ->
          log_file := Some s;
          if Option.is_none !log_level then log_level := Some Debug),
      "<file>: use <file> for logging." ;
    "-log-level", Arg.String set_level,
      (sprintf "<level>: set logging level to <level> (one of %a)"
        (List.pp ~sep:", " string) (List.map fst level_opts)) ;
    "-show-progress", Arg.Set show_progress,
      ": output progress updates on standard error."
  ]

let usage =
  sprintf" [-log-file <file>] [-log-level <%a>] [-show-progress]"
    (List.pp ~sep:"|" string) (List.map fst level_opts)
