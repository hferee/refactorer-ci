open Containers

open Compiler
open Asttypes
open Types

open Identifier

val unwrap :
  Typedtree.module_type
    -> Typedtree.module_type
        * (Path.t * Longident.t loc * Typedtree.with_constraint) list list

val module_type_consistent_with :
  Elements.Module._sub_t -> Elements.ModuleType._sub_t -> bool
(** [ST] is consistent with [S] and [FT] is consistent with [F]. *)

val sort : Env.t -> module_type -> Elements.ModuleType._sub_t Option.t
(** [module_type_sort env mt] returns an optional [_module_type] value
    corresponding to the sort of the module type [mt] (i.e. either a structure
    or a functor). If the sort cannot be determined, then [None] is returned.
    [env] is used to lookup modules and module types encountered when examining
    the structure of [mt], and thus should be the environment corresponding to
    the point in the AST where the [mt] parameter comes from. *)

open Types_views

val resolve : Env.t -> Compiler.Types.module_type -> _module_type

val resolve_view : Env.t -> 'a module_type_view -> 'a module_type
(** Resolve a module type view to a concrete module type; this may involve
    looking up module types and the types of modules referenced by
    identifiers, hence the need to provide an environment. *)

val lookup :
  Env.t -> 'a module_type -> 'b Chain.t -> 'b item_element_view Option.t
(** [lookup env mtv id] looks up the element referenced by [id] in the
    module type view [mtv]; this may require module (type) identifiers to
    be resolved, hence the need to provide the environment [env]. *)

val _lookup :
  Env.t -> _module_type -> 'b Chain.t -> 'b item_element_view Option.t

val resolve_lookup :
  Env.t -> Compiler.Types.module_type -> 'b Chain.t
    -> 'b item_element_view Option.t
(** [_lookup env mty id] first [resolve]s the module type [mty] and then
    performs [_lookup]. *)

val find : (Ident.t, 'a) Atom.t -> Env.t -> _module_type Option.t
(** [find a env] returns the module type of the module or module type identified
    by [a] by using the appropriate compiler lookup function on [env]. If the
    lookup fails, then [None] is returned. If [a] is not of the correct sort
    (i.e. a [_structure], [_functor], [_structure_type], or [_functor_type]
    atom), the Invalid_argument is raised. *)

val find_lookup :
  (Ident.t, 'a) Atom.t -> Env.t -> 'b Chain.t -> 'b item_element_view Option.t

val contains : Env.t -> 'a module_type -> (Ident.t, 'b) Atom.t -> bool
(** [contains env mt a] returns true if and only if [mt] contains a binding
    for the element [a]. *)

module Binding : sig
  val sort :
    Typedtree.module_type_declaration -> Elements.ModuleType._sub_t Option.t
end