open Containers
open Fun

open Compiler
open Asttypes

open Typedtree

open Module

open Elements
open Identifier

open Typedtree_views

type parameter_frame = (Ident.t * module_type) option * module_type list

type module_type_constraint = Path.t * Longident.t loc * with_constraint

type module_constraint =
  | InStr of module_type list
  | InSig of module_type_constraint list list

type frame =
  | Module of Ident.t option * module_constraint * parameter_frame list
  | ModuleType of
      Ident.t option * module_type_constraint list list * parameter_frame list
  | Parameter of Ident.t
(* TODO: Should we be using a [Types.module_type] list instead? *)

type t = (string * signature option) * frame list

let module_name ((n, _), _) = n
let module_sig ((_, _sig), _) = _sig

let default_toplevel_name = "default"

let empty = (default_toplevel_name, None), []

let pp_frame fmt =
  function
  | Module (None, _, _) ->
    Format.fprintf fmt "anonymous module"
  | Module (Some Ident.{ name; _}, _, _) ->
    Format.fprintf fmt "module binding %s" name
  | ModuleType (None, _, _) ->
    Format.fprintf fmt "anonymous module type"
  | ModuleType (Some Ident.{ name; _}, _, _) ->
    Format.fprintf fmt "module type binding %s" name
  | Parameter Ident.{ name; _ } ->
    Format.fprintf fmt "functor parameter %s" name

let peek (_, frames) =
  match frames with
  | [] -> invalid_arg (Format.sprintf "%s.peek" __MODULE__)
  | f::_ -> f

let frame_id =
  function
  | Module (id, _, _)
  | ModuleType (id, _, _) ->
    id
  | Parameter id ->
    Some id

let frame_is_naked =
  function
  | Module (_, InStr tys, params) ->
    List.is_empty tys
      &&
    List.for_all (fun (_, tys) -> List.is_empty tys) params
  | Module (_, InSig cs, params)
  | ModuleType (_, cs, params) ->
    List.is_empty (List.flatten cs)
      &&
    List.for_all (fun (_, tys) -> List.is_empty tys) params
  | Parameter _ ->
    true

let peek_module (_, fs) =
  match fs with
  | Module (id, cs, params) :: _ ->
    id, cs, params
  | _ ->
    invalid_arg (Format.sprintf "%s.peek_module" __MODULE__)

let peek_module_type (_, fs) =
  match fs with
  | ModuleType (id, cs, params) :: _ ->
    id, cs, params
  | _ ->
    invalid_arg (Format.sprintf "%s.peek_module" __MODULE__)

let peek_params (_, fs) =
  match fs with
  | Module (_, _, params) :: _
  | ModuleType (_, _, params) :: _ ->
    params
  | _ ->
    invalid_arg (Format.sprintf "%s.peek_params" __MODULE__)

let push_param p (top, fs) =
  match fs with
  | Module (m, tys, ps) :: rest ->
    (top, (Module (m, tys, p::ps)) :: rest)
  | ModuleType (mt, cs, ps) :: rest ->
    (top, (ModuleType (mt, cs, p::ps)) :: rest)
  | _ ->
    invalid_arg (Format.sprintf "%s.push_param" __MODULE__)

let enter_module_binding (type a) (mv : a module_view) (toplevel, frames) =
  match mv with
  | InStr { mb_id; mb_expr; _} ->
    let me, tys = unwrap_expr mb_expr in
    let scope = toplevel, (Module (Some mb_id, InStr tys, [])) :: frames in
    scope, ((InStr me) : a module_expr_view)
  | InSig { md_id; md_type; _ } ->
    let mt, cs = Moduletype.unwrap md_type in
    let scope = toplevel, (Module (Some md_id, InSig cs, [])) :: frames in
    scope, ((InSig mt) : a module_expr_view)

let enter_include (type a) (decl : a include_view) (toplevel, frames) =
  match decl with
  | InStr { incl_mod; _ } ->
    let e, tys = unwrap_expr incl_mod in
    let scope = toplevel, (Module (None, InStr tys, []))::frames in
    scope, ((InStr e) : a module_expr_view)
  | InSig { incl_mod; _ } ->
    let mt, cs = Moduletype.unwrap incl_mod in
    let scope = toplevel, (ModuleType (None, cs, []))::frames in
    scope, ((InSig incl_mod) : a module_expr_view)

let enter_module_type_declaration { mtd_id; mtd_type; _ } (toplevel, frames) =
  let pair = Option.map Moduletype.unwrap mtd_type in
  let mt, cs = Pair.(Option.map fst &&& Option.map_or ~default:[] snd) pair in
  let scope = toplevel, (ModuleType (Some mtd_id, cs, []))::frames in
  scope, mt

let enter_param p (toplevel, frames) =
  match frames with
  | []
  | Parameter _ :: _ ->
    invalid_arg (Format.sprintf "%s.enter_param" __MODULE__)
  | _ ->
    (toplevel, (Parameter p) :: frames)

let find_param id (toplevel, frames) =
  let rec find_param id frames =
    match frames with
    | [] ->
      None
    | Parameter _ :: frames
    | Module (_, _, []) :: frames
    | ModuleType (_, _, []) :: frames ->
      find_param id frames
    | Module (m, tys, ((Some (id', _), _) as param)::params) :: frames
        when Ident.same id id' ->
      Some (param, Module (m, tys, params) :: frames)
    | Module (m, tys, _::params) :: frames ->
      find_param id (Module (m, tys, params) :: frames)
    | ModuleType (mt, cs, ((Some (id', _), _) as param)::params) :: frames
        when Ident.same id id' ->
      Some (param, ModuleType (mt, cs, params) :: frames)
    | ModuleType (mt, cs, _::params) :: frames ->
      find_param id (ModuleType (mt, cs, params) :: frames) in
  Option.map Pair.(map2 (make toplevel)) (find_param id frames)

let in_frame_sigs id =
  function
  | Module  (_, InStr tys, params) ->
    let param_tys = List.flat_map snd params in
    tys @ param_tys
    |> List.exists @@ fun Typedtree.{ mty_env; mty_type; _ } ->
        begin match
          Moduletype.resolve_lookup mty_env mty_type id
        with
        | Some _ ->
          true
        | None ->
          false
        | exception Invalid_argument _ ->
          false
        | exception Not_found ->
          false
        end
  | _ ->
    invalid_arg (Format.sprintf "%s.in_frame_sigs" __MODULE__)

(* Processes a stack of frames and collects together parameter frames with their
   parent module (type) frames. *)
let collect frames =
  List.fold_left
    (fun acc f ->
      match f, acc with
      | _, [] ->
        [(f, None)]
      | ModuleType (Some _, _, _), (((Parameter p), None) :: frames)
      | Module (Some _, _, _), (((Parameter p), None) :: frames) ->
        (f, Some (Parameter p)) :: frames
      | Module _, (Module _, _) :: frames
      | Module _, (ModuleType _, _) :: frames
      | ModuleType _, (Module _, _) :: frames
      | ModuleType _, (ModuleType _, _) :: frames
      | Parameter _, (Module _, None) :: frames
      | Parameter _, (ModuleType _, None) :: frames
      ->
        (* TODO: Make this cleaner? i.e. not so many explicit cases. *)
        (f, None) :: acc
      | _ ->
        (* TODO: Need to revisit this because functor arguments CAN have functor types! *)
        invalid_arg (Format.sprintf "%s.collect" __MODULE__))
    []
    frames

let to_identifier ?(check_sigs=false) ?id_tail ((mod_name, _sig), frames) =
  let mk sort id = Atom._mk sort (Atom.Data.only id) in
  let mk_param id idx =
    Atom.Ex (Atom.mk Elements.Base.Parameter Atom.Data.{ id; idx; }) in
  let local_atms, frames =
    List.fold_map
      (fun atms (f, p) ->
        match f, p with
        | Module (None, _, []), None
        | ModuleType (None, _, []), None ->
          atms, (atms, f)
        | Module (Some Ident.{ name; _ }, _, params), None ->
          let m =
            if List.is_empty params
              then mk Elements.Base.(Ex Structure) name
              else mk Elements.Base.(Ex Functor) name in
          m::atms, (atms, f)
        | ModuleType (Some Ident.{ name; _ }, _, params), None ->
          let m =
            if List.is_empty params
              then mk Elements.Base.(Ex StructureType) name
              else mk Elements.Base.(Ex FunctorType) name in
          m::atms, (atms, f)
        | Module (Some Ident.{ name; _}, _, params),
            Some (Parameter Ident.{ name = id; _ }) ->
          let m = mk Elements.Base.(Ex Functor) name in
          let idx = Some ((List.length params) + 1) in
          let p = mk_param id idx in
          p::m::atms, (atms, f)
        | ModuleType (Some Ident.{ name; _}, _, params),
            Some (Parameter Ident.{ name = id; _ }) ->
          let m = mk Elements.Base.(Ex FunctorType) name in
          let idx = Some ((List.length params) + 1) in
          let p = mk_param id idx in
          p::m::atms, (atms, f)
      | _ ->
          invalid_arg
            (Format.sprintf "%s.to_identifier (line %i)" __MODULE__ __LINE__))
      []
      (collect frames) in
  let build =
    List.fold_left @@
      fun id_tail atm ->
        id_tail
          |> Option.map (let Atom.Ex atm = atm in Chain._cons atm)
          |> (Option.get_or ~default:(Chain._mk atm)) %> Option.return
      in
  let frames =
    frames
    |> List.rev_map @@ fun (atms, f)
    -> ((build id_tail atms), f) in
  let local_id = build id_tail local_atms in
  let full_id =
    let hd = Atom.mk Elements.Base.Structure (Atom.Data.only mod_name) in
    local_id
    |> Option.map (Chain._cons hd)
    |> (Option.get_or ~default:(Chain._mk (Atom.Ex hd))) %> Option.return in
  if not check_sigs then
    full_id
  else
    let frame =
      List.find_pred
        (fun (_, f) ->
          match f with
          | Module (_, InStr tys, params) ->
            not (List.is_empty tys)
              ||
            List.exists
              (fun (_, tys) -> not (List.is_empty tys))
              (params)
          | _ ->
            false)
        (frames) in
    match frame, _sig, local_id with
    | Some (Some (Identifier.Chain.Ex (_, id)), f), _, _
        when in_frame_sigs id f ->
      full_id
    | None, Some _sig, Some (Identifier.Chain.Ex (_, local_id))
      (* TODO: Review handling of anonymous module frames *)
        when
          local_id
          |> Moduletype.lookup
              (_sig.Typedtree.sig_final_env)
              (Types_views.ST _sig.Typedtree.sig_type)
          |> Option.is_some ->
      full_id
    | _ ->
      None