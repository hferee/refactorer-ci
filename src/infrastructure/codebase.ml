open Containers
open Format
open Fun

open Configuration

open Sourcefile
open Fileinfos
open Lib

include Fileinfos.Set
(* TODO: Implement something more efficient for a filesystem,
         e.g. a btree that mirrors the file structure. *)

let add_directory ~lib ~ppx ~dir cb =
  let printer = (fun f () -> fprintf f "Adding %s" dir) in
  if not (Sys.is_directory dir) then
    let () = Logging.info @@ fun _f -> _f "%a" printer () in
    raise Not_found
  else
    let files = Sys.readdir dir in
    let printer =
      (fun f () -> fprintf f
        "%a@,Found %i files" printer () (Array.length files)) in
    let files, printer =
      (([], printer), files)
        |> uncurry @@ Array.fold @@ fun (fs, printer) f ->
        let f = Filename.concat dir f in
        try if is_ocaml f then
          (Fileinfos.mk (f, lib, ppx) :: fs), printer
        else
          fs,
          (fun _f () -> fprintf _f "%a@,%s is not an ocaml file" printer () f)
        with Not_found ->
          fs,
          (fun _f () -> fprintf _f "%a@,%s was not found!" printer () f) in
    let printer =
      (fun f () -> fprintf f "%a@,Filtered down to %i files@]"
        printer () (List.length files)) in
    let () = Logging.info @@ fun _f -> _f "@[<v 4>%a@]" printer () in
    add_list cb files

let match_file ~suf f =
  let f = f.filename in
  String.suffix ~suf f
    && String.equal (Filename.basename suf) (Filename.basename f)
  (* TODO: There is still a potential bug here because we don't make sure that
           the suffix match is only carried out on subdirectory boundaries. Some
           e.g. foo/bar/baz.ml could still match with etc/blah_foo/bar/baz.ml *)

let find_file ?lib suf fs =
  let pred =
    Option.map_or ~default:(match_file ~suf)
      (fun lib f ->
        match_file ~suf f && Option.equal String.equal lib f.library)
      (lib) in
  List.find_pred pred (to_list fs)

let find_all_files suf fs =
  to_list (filter (match_file ~suf) fs)

(* TODO: is it more efficient to create the submodule here at the module top-
   level than locally in the dependency_graph function? *)
module CmtMap = CCHashtbl.Make(String)
let dependency_graph fs =
  let () =
    Logging.progress_statement @@ fun _f -> _f
      "Calculating file dependencies " in
  let num_files = cardinal fs in
  let map = CmtMap.create (num_files / 2) in
  let () =
    fs |> iter @@ fun f
       -> let mod_name = cmt_module_name f in
          CmtMap.replace map mod_name
            (f :: (CmtMap.get_or ~default:[] map mod_name)) in
  let graph = Graph.create ~size:num_files () in
  let () =
    fs |> iter @@ fun f ->
      let () = Graph.add_vertex graph f in
      let deps =
        try
          Sourcefile.get_module_deps (of_fileinfos f)
        with Invalid_argument _ ->
          (* TODO: Eventually, need to handle case when typed tree not available? *)
          let () = Logging.err @@ fun _f -> _f
            ~header:"File Deps" "No typed AST for %s" f.filename in
          failwith (Format.sprintf "No typed AST for %s" f.filename) in
      let () = Logging.info @@ fun _f -> _f
        "@[<v 4>Found %i dependencies for %a%a%a@]"
          (List.length deps)
          pp_filename f
          (if List.is_empty deps then silent else return "@,") ()
          (List.pp pp_print_string) deps in
      let () = Logging.minor_progress () in
      deps
      |> List.flat_map (CmtMap.get_or ~default:[] map)
      |> begin tap @@ fun fs ->
           if not (List.is_empty fs) then
             Logging.info @@ fun _f -> _f
               "@[<v 4>converted to the following file dependencies:@,%a@]"
               (List.pp pp_filename) fs
         end
      |> List.iter (Graph.add_edge graph f) in
  let () = Logging.major_progress () in
  graph