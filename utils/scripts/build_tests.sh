#!/bin/bash

# The file containing the identifiers to run tests over is expected as the first
# parameter of the script. We check that it is accessible

if [ $# -lt 1 ]; then
  echo "Please specify a file containing the test cases to be run!"
  exit 1
fi

TEST_CASE_FILE=$1

if [ ! -f "$TEST_CASE_FILE" ]; then
  echo "Cannot find test case file!"
  exit 1
fi

# The directory in which to store the results is expected as the second
# parameter of the script. We check that it is accessible.

if [ $# -lt 2 ]; then
  echo "Please specify a directory for storing the results!"
  exit 1
fi

# Keep a record of the base directory.

BASE_DIR=$(pwd)

RESULTS_DIR="$BASE_DIR/$2"

if [ ! -d "$RESULTS_DIR" ]; then
  echo "Results directory does not exist!"
  exit 1
fi

# We need to know where the testbed is.

if [ ! -d "$JANE_STREET_PATH" ]; then
  echo "Cannot find Jane Street test bed!"
  exit 1
fi

if [ -n "$REFACTOR_ONLY" ]; then exit 0; fi

cd "$RESULTS_DIR" || exit 1

while read ID COUNT || [[ -n "$ID" ]]; do
    PATCH_FILE="$ID.patch"
    if [ -f "$PATCH_FILE" ]; then
        TMP_DIR=$(mktemp -d)
        cp -R "$JANE_STREET_PATH" "$TMP_DIR/code"
        cd "$TMP_DIR/code" || exit
        jbuilder clean
        patch -p 0 < "$RESULTS_DIR/$PATCH_FILE" > /dev/null 2>&1
        if jbuilder build > /dev/null 2>&1 ; then
          cp ./_build/log "$RESULTS_DIR/$ID.failed"
          echo "$ID BUILDFAILED"
        else
            echo "$ID BUILD SUCCEEDED"
        fi
        rm -fR "$TMP_DIR"
    fi
done < "$BASE_DIR/$TEST_CASE_FILE"
