#!/bin/bash

if [ $# -lt 1 ]; then
  echo "Please specify test cases to be run!"
  exit 1
fi

ID=${$1%.patch}

PATCH_FILE="$RESULTS_DIR/$1"

# Keep a record of the base directory.
BASE_DIR=$(pwd)

RESULTS_DIR="$BASE_DIR/tmp"


JANE_STREET_PATH="/var/lib/gitlab-runner/js-testbed"


# Now test the build

TMP_DIR=$(mktemp -d)
cp -R "$JANE_STREET_PATH" "$TMP_DIR/code"
cd "$TMP_DIR/code" || exit
jbuilder clean
patch -p 0 < "$PATCH_FILE" > /dev/null 2>&1

if jbuilder build > /dev/null 2>&1 ; then
  cp ./_build/log "$RESULTS_DIR/$ID.failed"
  echo "$ID BUILD_FAILED"
  rm -fR "$TMP_DIR"
  exit 1
fi

echo "$ID SUCCEEDED"
rm -fR "$TMP_DIR"
