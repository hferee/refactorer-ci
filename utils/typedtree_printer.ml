open Format

open Compiler

open Compmisc

open Frontend
open Sourcefile
open Fileinfos

open Logging

open Containers.Fun

module type Printer =
sig
  val interface : Format.formatter -> Typedtree.signature -> unit
  val implementation: Format.formatter -> Typedtree.structure -> unit
end

module Untype = struct
  let interface fmt =
    Untypeast.untype_signature %> Printast.interface fmt
  let implementation fmt =
    Untypeast.untype_structure %> Printast.implementation fmt
end

module UntypePP = struct
  let interface fmt =
    Untypeast.untype_signature %> Pprintast.signature fmt
  let implementation fmt =
    Untypeast.untype_structure %> Pprintast.structure fmt
end

let printer = ref (module Printtyped : Printer)

let speclist =
  Frontend.speclist @
  Logging.speclist @
  [
    "-untype", Arg.Unit (fun () -> printer := (module Untype : Printer)),
      ": convert the typed AST into a parse tree" ;
    "-untype-pp", Arg.Unit (fun () -> printer := (module UntypePP : Printer)),
      ": convert the typed AST into a parse tree and pretty print" ;
  ]

let () = Arg.parse speclist (fun _ -> ()) usage

let () =
  Logs.set_reporter (Logging.reporter ()) ;
  Logs.set_level !Logging.log_level

let () =
  init_path false ;
  let codebase = Lazy.force codebase in
  let () =
      if Codebase.cardinal codebase = 0
        then failwith "No input file specified!" in
  codebase |> Codebase.iter @@ of_fileinfos %> (fun f ->
  print_endline f.fileinfos.filename ;
  let module Printer = (val !printer : Printer) in
  match f.ast with
  | Interface (_, Some _sig) ->
      Printer.interface std_formatter _sig
  | Implementation (_, Some _struct) ->
      Printer.implementation std_formatter _struct
  | _ ->
      prerr_endline "No AST found!")
