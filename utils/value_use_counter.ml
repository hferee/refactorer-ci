open Containers

open Compiler

open Compmisc
open Config

open Typedtree

open Frontend

open Sourcefile

module Htbl = Hashtbl.Make'(String)

let () = Arg.parse speclist (fun _ -> ()) usage

let () = init_path false

let codebase = Lazy.force codebase

let ident_map : int Htbl.t = Htbl.create 1000

let () =
  let iter =
    object (self)
      inherit [_] Typedtree_visitors.iter as super
      method! visit_tt_expression env e =
        match e.exp_desc with
        | Texp_ident (p, _, _) ->
          let p = Env.normalize_path (Some e.exp_loc) e.exp_env p in
          if Ident.persistent (Path.head p) then
          begin try
            let _ = Env.find_value p e.exp_env in
            let p = Printtyp.string_of_path p in
            let n = Htbl.get_or ~default:1 ident_map p in
            Htbl.replace ident_map p (n + 1)
          with Not_found -> () end
        | _ ->
          super#visit_tt_expression env e
    end in
  codebase |> Codebase.iter @@ fun f ->
    match (of_fileinfos f).ast with
    | Interface (_, Some _sig) ->
      iter#visit_tt_signature () _sig
    | Implementation (_, Some _struct) ->
      iter#visit_tt_structure () _struct
    | _ ->
      failwith
        (Format.sprintf
          "No typed AST for %a" Fileinfos.pp_filename f)

let () =
  Htbl.iter
    (Format.fprintf Format.std_formatter "%s %i@.")
    ident_map
