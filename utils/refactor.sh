#!/bin/bash

# This script is a wrapper around the rotor (main) executable. Its purpose is to
# read the include and input directories from the specified files and generate
# the appropriate command line options for the rotor executable. Other
# parameters are passed as-is to the executable so, in particular, the
# refactoring can be specified by the user.

# Arguments to this script are as follows
#
# $1: A file containing the include directories, one per line
# $2: A file containing a list of source input directories along with the name
#       of the library to which the directory corresponds, one pair per line.
#
# The remainder are treated as arguments to the refactoring

# Executable name - works relative to the location of the script
#   (assuming script is called directly, and not found automatically in the path)
SCRIPTNAME="${0##*/}"
CURRENTDIR=${0%$SCRIPTNAME}
EXE="$CURRENTDIR../rotor"

# usage
USAGE="${0} <include-dirs-spec-file> <input-dirs-spec-file> <args>"

# Can we execute the refactoring program?
if [ ! -x "$EXE" ]; then
  echo "\"$EXE\" cannot be found, or is not executable!"
  exit 1
fi

# Do we have enough arguments?
if [ $# -lt 2 ]; then
  echo "usage: $USAGE"
  exit 1
fi

INCLUDE_SPECS=$1
shift
INPUTSPECS=$1
shift

# Generate include options
INCLUDE_PARAMS=""
while read INCLUDE || [[ -n "$INCLUDE" ]]; do
    INCLUDE_PARAMS="$INCLUDE_PARAMS -I $INCLUDE"
done < "$INCLUDE_SPECS"

#Generate input options
INPUT_DIRS=""
while read INPUTDIR LIB || [[ -n "$INPUTDIR" ]]; do
    INPUT_DIRS="$INPUT_DIRS -d $INPUTDIR"
    if [[ -n "$LIB" ]]; then
        INPUT_DIRS="$INPUT_DIRS -lib $LIB"
    fi
done < "$INPUTSPECS"

# Call the executable
"$EXE" $INCLUDE_PARAMS $INPUT_DIRS $*
